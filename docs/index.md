# Home

## OpenLink - A gateway for research data management plateform

OpenLink is a web application designed to provide the researcher with a clear view of the data associated with each research project.
It reduces the barriers to adopting the FAIR principles and assists researchers in publishing data

OpenLink let's you create links between the structure of a research project described using the [ISA model](https://isa-specs.readthedocs.io/en/latest/isamodel.html) (Investigatio, Study, Assay) and multiple data sources.
It supports an evolving collection of data sources including LabGuru, Omero, Seafile, SSH endpoints and Zenodo. It also embed features that facilitate data manipulation and assist researchers in publishing their data.

OpenLink has been developped within Institut de Génétique et de Biologie Moléculaire et Cellulaire ([IGBMC](https://www.igbmc.fr/)) at Strasbourg, France. 
The proof of concept has been funded by ANR in 2019 and since 2021, The French Institut of Bioinformatic ([IFB](https://www.france-bioinformatique.fr/)) is funding OpenLink in order to make it a more mature product.


## License

OpenLink is released under the GPLv3 license. See the [LICENSE](https://gitlab.com/ifb-elixirfr/openlink/openlink/-/blob/master/LICENSE) file.
