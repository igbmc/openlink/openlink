# Configuration

OpenLink can be configured via an environment file.

By default Openlink will look for the `openlink.env` file located at `/etc/openlink/`. However, you can specify the path to an environment file using the `OPENLINK_ENV_PATH` environment variable

## Configuration variables

### Base settings
| **Name**                | **Description**                                                                                                                                                                                                                                                  |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `SECRET_KEY`              | This is used to provide cryptographic signing, and should be set to a unique, unpredictable value.  [See here](https://docs.djangoproject.com/en/3.1/ref/settings/#secret-key). If you don't provide a key this one will be generated each time OpenLink starts. |
| `DEBUG`                   | Turn on/off debug mode. Never deploy your OpenLink instance into production with DEBUG turned on.                                                                                                                                                                |
| `DATABASE_URL`            | The database connection url string                                                                                                                                                                                                                               |
| `INSTALLED_CONNECTORS`    | List of OpenLink connector apps you want to enable on your OpenLink instance. You can see the section about [Adding a connector to your openlink instance](connector/adding_connector.md#adding-a-connector-to-your-openlink-instance) for further details.                                                                                                                                                                                    |

### LDAP Authentication settings

The following settings configure LDAP authentification in OpenLink. By default LDAP is disabled.

!!! warning "Required"
    LDAP authentication requires `python-ldap` and `django_auth_ldap`.

    ```bash
    pip install python-ldap django_auth_ldap
    ```
    Please, read [python-ldap documentation](https://www.python-ldap.org/en/python-ldap-3.3.0/installing.html) for more details on required packages for `python-ldap` building.

| **Name**                | **Description**                                                                                                                                                                                                                                                  |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `AUTH`               | type 'LDAP' to enable LDAP authentication                                      |
| `AUTH_LDAP_SERVER_URI`    | URI of LDAP server                                              |
| `AUTH_LDAP_BIND_DN`       | name to use when binding to the LDAP server                     |
| `AUTH_LDAP_BIND_PASSWORD` | Password for   AUTH_LDAP_BIND_DN                                |
| `AUTH_LDAP_SEARCH`        | LDAPSearch argument to use on AUTH_LDAP_USER_SEARCH variable    |
| `AUTH_LDAP_USER_ATTR_MAP` | Ldap attributs retrieved for a user                             |

### OIDC authentication settings

The following settings configure OpenId Connect authentification in OpenLink. By default OIDC is disabled.

!!! warning "Required"
    OpenID Connect authentication requires `mozilla_django_oidc`.

    ```bash
    pip install mozilla_django_oidc
    ```

| **Name**                | **Description**                                                                                                                                                                                                                                                  |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `AUTH`                             | type 'OIDC' to enable OpenId Connect                            |
| `OIDC_RP_CLIENT_ID`                     | Client ID                                        |
| `OIDC_RP_CLIENT_SECRET`                 | Client secret                                    |
| `OIDC_OP_AUTHORIZATION_ENDPOINT`        | OAuth2 authorization endpoint                    |
| `OIDC_OP_TOKEN_ENDPOINT`                | OAuth2 token endpoint                            |
| `OIDC_OP_USER_ENDPOINT`                 | OAuth2 userinfo endpoint                         |
| `OIDC_OP_JWKS_ENDPOINT`                     | URL of JWKS endpoint                              |
`OIDC_STORE_ACCESS_TOKEN`                     | 'TRUE' to keep the OIDC access token in session in order to be able to perform JWT authentication on Vault                              |
`OIDC_JWT_ROLE`                 | jwt role                             |