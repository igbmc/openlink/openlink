# Vault

Openlink use vault by hashicorp as a secure credentials storage and as an authentication backend.

First step [install vault](https://www.vaultproject.io/):
Multiple tutorials and installation documentation can be found on there site, here the [quick start](https://learn.hashicorp.com/tutorials/vault/getting-started-install?in=vault/getting-started-ui) one.

Vault for Openlink can be configured via the environment file.

By default Openlink will look for the `openlink.env` file located at `/etc/openlink/`. However, you can specify the path to an environment file using the `OPENLINK_ENV_PATH` environment variable

## Create Policies on Vault

The following access policies must be set up by the Vault administrator:

openlink_user:

```
path "openlink/{{identity.entity.id}}/*" {
  capabilities = ["create", "update", "read", "delete", "list"]
}
path "openlink/{{identity.entity.id}}" {
  capabilities = ["create", "update", "read", "delete", "list"]
}
path "openlink/shared/{{identity.entity.id}}/*" {
  capabilities = ["create", "update", "read", "delete", "list"]
}
path "openlink/shared/{{identity.entity.id}}" {
  capabilities = ["create", "update", "read", "delete", "list"]
}
```

openlink_worker:

```
path "openlink/shared/*" {
  capabilities = ["read", "list"]
}
```

## OpenIDConnect
It is necessary to enable JWT authentication on the Vault server

```
vault write auth/jwt/config oidc_discovery_url="" oidc_client_id="" oidc_client_secret="" bound_issuer=""
```

You must also create a role for OpenLink users giving them access to the policy

openlink_user:

```
vault write auth/jwt/role/openlink_user policies=openlink_user user_claim=sub role_type=jwt bound_audiences=openlink
```

## Approle

OpenLink workers must use AppRole authentication to access the Vault.
Create an openlink approle in vault with a openlink_worker role.
Then, attach openlink_worker policy to it.


## Configuration variables

### Base settings
| **Name**                | **Description**                                                                                                                                                                                                                                                  |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `AUTH_PATH_LIST`              | list of authentification methode declared in your vault |
| `VAULT_HOSTNAME`                   | Vault host url                                                                                                                                                                |
| `VAULT_PORT`            | Port used for Vault                                                                                                                                                                                                                              |
| `VAULT_SECRET_ID`            | The secret id given by vault to give access to the openlink approle (async tasks)                                                                                                                                                                                                                               |
| `VAULT_ROLE_ID`            | Vault role id given by vault to give access to the openlink approle (async tasks)                                                                                                 |                                      

  