# Installation

This section describes the best way to install Openlink.

Openlink releases can be downloaded from [Pypi]() or on [GitLab](https://gitlab.com/ifb-elixirfr/openlink)

## Requirements

Openlink requires Python 3.6 or later

## Install openlink package

The recommended way of installing Openlink is via pip inside a virtual environment

```bash
python3 -m venv venv
source venv/bin/activate
pip install ifb-openlink
```

## Initializing the Openlink database

OpenLink supports MariaDB/MySQL, PostgreSQL, and SQLite. See the complete guide on [configuring Openlink](configuration.md) for more details. By default, Openlink will use SQLite and create a database file in your current working directory.

After configuring your database of choice you must first initialize the Openlink database. This should only be done once:

```bash
openlink migrate
```

## Add a superuser and start openlink server

Add a superuser account in your database by running the following command:

```bash
openlink createsuperuser
```

## Start Redis Queue worker

Openlink is able to handle asynchronous tasks in the background using RQ workers.
You can find documentation on installing a Redis server on [the Redis website](https://redis.io/topics/quickstart)

Once Redis is installed and runnning, start OpenLink RQ worker with the following command:

```bash
python openlink rqworker default
```

## Running Openlink server

Use the following command to quickly start the developpement web server:

```bash
DEBUG=True openlink runserver
```

OpenLink is now running.

Point your browser to [http://localhost:8000](http://localhost:8000) and login with the super user account you created.

!!! danger "Danger"
    Do not run this in production.

    In production, Openlink static assets must be served via a web server such as nginx. For more information [see here](https://docs.djangoproject.com/en/4.0/howto/static-files/#deployment).
    Then, configure the settings in an openlink env file before using the following command:
    ```bash
    openlink runserver
    ```
    For more details on deploying Openlink in production, see the complete guide on [configuring Openlink](configuration.md).