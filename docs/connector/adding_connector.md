# Adding a connector

Openlink comes with the following builtins connectors :

| **Connector** | **Type** | **Description**                                                                | **Package**  |
|---------------|----------|--------------------------------------------------------------------------------|---|
| Labguru       | Mapper   | Create links to LabGuru projets, folders or experiments                        | `openlink.connectors.openlink_labguru` |
| Omero         | Mapper   | Create links to Omero projects or datas                                     | `openlink.connectors.openlink_omero` |
| Seafile       | Mapper   | Create links to Seafile libraries, folders or files                            | `openlink.connectors.openlink_seafile` |
| SSH           | Mapper   | Create links to folders or files stored on data storage accessible through SSH | `openlink.connectors.openlink_ssh` |
| Zenodo        | Publish  | Publish datas to Zenodo                                                     | `openlink.connectors.openlink_zenodo` |

You can specify the connector you want to enable on your OpenLink instance by listing them in the `INSTALLED_CONNECTORS` variables in your OpenLink environment file. See [configuration](../configuration.md).

You can also [create your own connector](creating_connector.md).
