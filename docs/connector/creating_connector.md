# Creating a new connector

Openlink give you the opportunity to create your own connectors so that you can establish data links to custom data sources (Mapper) and publish your data in custom repositories (Publisher).

A connector is a simple Django application that contains the Mapper or Publisher implementation in its code.

## Create a Django app

First, create a Django app for your connector:
```
openlink startapp my_connector
```

## Write the `connector.py` module

Then, add a python module `connector.py` in your app package.

Your `connector.py` module should implement a class that inherites from the OpenLink Mapper or Publisher class.\
Description, arguments and expected values are detailed in the [API Reference](api_reference.md#api-reference) section.


<!--
Another class initiate the form to add a new instance of your connector.
This class inherite from parent class "ToolForm".
First, declare the django form fields needed for tool connexion (hostname, username, token, password, ...)
Then, write a method "clean" where you retrieve inputs from "cleaned_data" and try to connect to a tool account and raise an exception if there are Authentication/Connexion errors.

Another class handles the edit form.
This class also inherite from parent class "ToolForm".
First, declare the django field about token or password used for tool connexion.
Then, write a method "clean" where you retrieve inputs from "cleaned_data" and try to connect to a tool account with the updated credentials and raise exception if any error occurs.
You should also add a method "set_url" that sets "self.url".
Refers to already existing connector classes like the ones used for Labguru as an exemple ("LabGuruForm" and "LabgurueditForm" in openlink/connectors/openlink_labguru/connectors.py).

When your connector is ready, declare your app into the INSTALLED_CONNECTORS variable in your OpenLink environment file (see [configuration](configuration.md))
-->
