# API Reference

## Shared connector methods

::: openlink.core.connector.ToolConnector

## Mapper connector methods

::: openlink.core.connector.Mapper

## Publisher connector methods

::: openlink.core.connector.Publisher