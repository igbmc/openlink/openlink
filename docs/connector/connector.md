# Introduction

Openlink can connect and create link to any data source and publish them on a repository. The only condition is to have a connector setup in your Openlink project.

A connector dictate how OpenLink can communicate with a given data source or data repository.

They are 2 types of connectors: Mappers and Publishers.

A Mapper is able to map data on an Openlink project ISA model as links.

A Publisher can archive data previously mapped to an Openlink project and publish them on a specified respository.
