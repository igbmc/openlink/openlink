import logging
import os

import hvac
from django import forms
import django.conf
from django.conf import settings
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from openlink.core.models import Profile, TaskInfo

logger = logging.getLogger(__name__)
vault_url = settings.AUTH_VAULT_CONFIG["VAULT_HOSTNAME"] + settings.AUTH_VAULT_CONFIG["VAULT_PORT"]


class UserLoginForm(AuthenticationForm):
    username = UsernameField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': '', 'id': 'login_username'}))
    password = forms.CharField(required=False, widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': '', 'id': 'login_password'}))
    token = forms.CharField(required=False, widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': '', 'id': 'login_token'}))
    oidc_role = forms.CharField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': '', 'id': 'login_oidc_role'}))

    def __init__(self, request=None, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)
        self.request = request
        self.user_cache = None

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        token = self.cleaned_data.get('token')
        oidc_role = self.cleaned_data.get('oidc_role')

        if username or password or token or oidc_role:
            self.user_cache = authenticate(self.request, username=username, password=password, token=token, role=oidc_role)
            if self.user_cache is None:
                self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)
        return self.cleaned_data


@login_required
def custom_logout(request):
    current_user = Profile.objects.get(user=request.user)
    if "vault_token" in request.session and TaskInfo.objects.filter(author=current_user) is None:
        try:
            vault_client = hvac.Client(url=vault_url, token=request.session['vault_token'])
            vault_client.logout(revoke_token=True)
            print("vault token revoked")
        except Exception as e:
            logger.debug("couldn't revoke vault  token")
            logger.debug(e)
    logout(request)
    return render(request, "registration/logged_out.html")


def vault_credentials_check(request):
    current_user = Profile.objects.get(user=request.user)
    vault_client = hvac.Client(url=vault_url, token=request.session["vault_token"])
    try:
        list_tool_linked = list(current_user.tool_list.all())
        list_tool_registered = list(vault_client.secrets.kv.v1.list_secrets(mount_point="openlink", path=current_user.vault_id)['data']['keys'])
        for tool_id in list_tool_registered:
            if str(tool_id) not in str(list_tool_linked):
                vault_client.secrets.kv.v1.delete_secret(mount_point="openlink", path=os.path.join(current_user.vault_id, tool_id))
                current_user.tool_list.remove(tool_id)
        list_tool_linked_shared = current_user.tool_list_shared.all()
        list_tool_registered_shared = vault_client.secrets.kv.v1.list_secrets(mount_point="openlink", path=os.path.join("shared", current_user.vault_id))['data']['keys']
        for tool_id in list_tool_registered_shared:
            if str(tool_id) not in str(list_tool_linked_shared):
                vault_client.secrets.kv.v1.delete_secret(mount_point="openlink", path=os.path.join("shared", current_user.vault_id, tool_id))
                current_user.tool_list_shared.remove(tool_id)
    except Exception as e:
        logger.debug("Couldn't check tool validity")
        logger.debug(e)


def login_page(request):
    if request.method == "POST":
        if 'userpass_submit_btn' in request.POST:
            type = "userpass"
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request=request, username=username, password=password, type=type)
            if user:
                if user.is_active:
                    login(request, user)
                    profile = Profile.objects.get(user=user)
                    # check vault login
                    client = hvac.Client(url=vault_url)
                    reponse = client.auth.ldap.login(
                        username=username,
                        password=password,
                        mount_point='ldap'
                    )
                    request.session["vault_token"] = reponse["auth"]["client_token"]
                    profile.vault_id = str(reponse["auth"]["entity_id"])
                    profile.save()
                    vault_credentials_check(request)

                    return redirect(reverse('core:home'))
            else:
                messages.error(request, 'username or password not correct')
                return redirect(reverse('login'))
    form_userpass = AuthenticationForm()
    return render(request, "registration/login.html", {'form': form_userpass})