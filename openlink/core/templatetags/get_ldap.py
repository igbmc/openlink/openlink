from django import template
from django.conf import settings

register = template.Library()


@register.filter(name="get_ldap")
def get_ldap(val=None):
    return settings.AUTH_LDAP
