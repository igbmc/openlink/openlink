import logging

import django.dispatch
import hvac
from django.apps import apps
from django.conf import settings
from django.contrib.auth.signals import user_logged_in
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from openlink.core.models import Mapping, Profile, Tool
from openlink.log_manager import vault_credentials_check

logger = logging.getLogger(__name__)

signal = django.dispatch.Signal()
new_mapping_signal = django.dispatch.Signal()
delete_signal = django.dispatch.Signal()


@receiver(pre_delete, sender=Mapping)
def delete__signal(sender, **kwargs):
    mapping = kwargs["instance"]
    item = apps.get_model("core", str(mapping.type)).objects.get(
        id=mapping.foreign_id_obj.id
    )
    connector = Tool.objects.get(mapping__id=mapping.id).get_connector(None)
    delete_signal.send_robust(
        apps.get_model("core", str(mapping.type)),
        del_mapping=mapping,
        item=item,
        connector=connector,
    )


@receiver(post_save, sender=Mapping)
def new_map_signal(sender, **kwargs):
    mapping = kwargs["instance"]
    item = apps.get_model("core", str(mapping.type)).objects.get(
        id=mapping.foreign_id_obj.id
    )
    connector = Tool.objects.get(mapping__id=mapping.id).get_connector(None)
    new_mapping_signal.send_robust(
        apps.get_model("core", str(mapping.type)),
        new_mapping=mapping,
        item=item,
        connector=connector,
    )


@receiver(user_logged_in)
def add_vault_token_to_session(sender, user, request, **kwargs):
    if "oidc_access_token" in request.session:
        vault_url = (
            settings.AUTH_VAULT_CONFIG["VAULT_HOSTNAME"]
            + settings.AUTH_VAULT_CONFIG["VAULT_PORT"]
        )
        oidc_token = request.session["oidc_access_token"]
        vault_client = hvac.Client(url=vault_url)
        response = vault_client.auth.jwt.jwt_login(
            role=settings.OIDC_JWT_ROLE,
            jwt=oidc_token,
        )
        profile = Profile.objects.get(user=user)
        profile.vault_id = str(response["auth"]["entity_id"])
        profile.save()
        request.session["vault_token"] = response["auth"]["client_token"]
        vault_credentials_check(request)
