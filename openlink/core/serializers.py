from openlink.core.models import (
    Assay,
    Data,
    Investigation,
    Mappableobject,
    Mapping,
    Study,
    TaskInfo,
)
from rest_framework import serializers


class MappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mapping
        fields = ["name", "tool_id", "object_id", "foreign_id_obj"]
        depth = 2


class TaskinfoSerializer(serializers.ModelSerializer):
    get_error = serializers.CharField()
    get_project_name = serializers.CharField()
    get_author_name = serializers.CharField()
    get_target_name = serializers.CharField()

    class Meta:
        model = TaskInfo
        fields = (
            "taskid",
            "description",
            "status",
            "result",
            "target",
            "author",
            "project",
            "start_date",
            "end_date",
            "get_error",
            "get_project_name",
            "get_author_name",
            "get_target_name",
            "get_target_url",
        )


class MappableobjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mappableobject
        fields = ["id"]
        depth = 1


class DataSerialiser(MappableobjectSerializer):
    info = serializers.SerializerMethodField()

    class Meta(MappableobjectSerializer.Meta):
        model = Data
        fields = ["info"]

    def get_info(self, obj):
        list_map = []

        qs_maps = Mapping.objects.filter(foreign_id_obj=obj.id)
        for qs_map in qs_maps:
            obj_type = qs_map.type
            obj_id = qs_map.object_id
            connector = qs_map.tool_id.get_connector()
            txt_to_add = {
                "tool": qs_map.tool_id.name,
                "tool_id": qs_map.tool_id.id,
                "obj_id": obj_id,
                "url": connector.get_url_link_to_an_object(obj_type, obj_id),
            }
            list_map.append(txt_to_add)

        return {
            "type": obj_type,
            "id": obj.pk,
            "name": obj.name,
            "mapping": list_map,
        }


class AssaySerializer(MappableobjectSerializer):
    data = DataSerialiser(many=True)
    info = serializers.SerializerMethodField()

    class Meta(MappableobjectSerializer.Meta):
        model = Assay
        fields = ["info", "data"]

    def get_info(self, obj):
        list_map = []

        qs_maps = Mapping.objects.filter(foreign_id_obj=obj.id)
        for qs_map in qs_maps:
            obj_type = qs_map.type
            obj_id = qs_map.object_id
            connector = qs_map.tool_id.get_connector()
            txt_to_add = {
                "tool": qs_map.tool_id.name,
                "tool_id": qs_map.tool_id.id,
                "obj_id": obj_id,
                "url": connector.get_url_link_to_an_object(obj_type, obj_id),
            }
            list_map.append(txt_to_add)

        return {
            "type": obj_type,
            "id": obj.pk,
            "name": obj.name,
            "mapping": list_map,
        }


class StudySerializer(MappableobjectSerializer):
    assay = AssaySerializer(many=True)
    info = serializers.SerializerMethodField()

    class Meta(MappableobjectSerializer.Meta):
        model = Study
        fields = ["info", "assay"]

    def get_info(self, obj):
        list_map = []

        qs_maps = Mapping.objects.filter(foreign_id_obj=obj.id)
        for qs_map in qs_maps:
            obj_type = qs_map.type
            obj_id = qs_map.object_id
            connector = qs_map.tool_id.get_connector()
            txt_to_add = {
                "tool": qs_map.tool_id.name,
                "tool_id": qs_map.tool_id.id,
                "obj_id": obj_id,
                "url": connector.get_url_link_to_an_object(obj_type, obj_id),
            }
            list_map.append(txt_to_add)

        return {
            "type": obj_type,
            "id": obj.pk,
            "name": obj.name,
            "mapping": list_map,
        }


class InvestigationSerializer(MappableobjectSerializer):
    study = StudySerializer(many=True)
    data = DataSerialiser(many=True)
    info = serializers.SerializerMethodField()

    class Meta(MappableobjectSerializer.Meta):
        model = Investigation
        fields = ["info", "study", "data"]

    def get_info(self, obj):
        list_map = []
        qs_maps = Mapping.objects.filter(foreign_id_obj=obj.id)
        for qs_map in qs_maps:
            obj_type = qs_map.type
            obj_id = qs_map.object_id
            connector = qs_map.tool_id.get_connector()
            txt_to_add = {
                "tool": qs_map.tool_id.name,
                "tool_id": qs_map.tool_id.id,
                "obj_id": obj_id,
                "url": connector.get_url_link_to_an_object(obj_type, obj_id),
            }
            list_map.append(txt_to_add)

        return {
            "type": "investigation",
            "id": obj.pk,
            "name": obj.name,
            "mapping": list_map,
        }


class ProjectSerializer(MappableobjectSerializer):
    investigation = InvestigationSerializer(many=True)
    info = serializers.SerializerMethodField()

    class Meta(MappableobjectSerializer.Meta):
        model = Investigation
        fields = ["info", "investigation"]

    def get_info(self, obj):
        list_map = []
        qs_maps = Mapping.objects.filter(foreign_id_obj=obj.id)
        for qs_map in qs_maps:
            obj_type = qs_map.type
            obj_id = qs_map.object_id
            connector = qs_map.tool_id.get_connector()
            txt_to_add = {
                "tool": qs_map.tool_id.name,
                "tool_id": qs_map.tool_id.id,
                "obj_id": obj_id,
                "url": connector.get_url_link_to_an_object(obj_type, obj_id),
            }
            list_map.append(txt_to_add)

        return {
            "type": "project",
            "id": obj.pk,
            "name": obj.name,
            "mapping": list_map,
        }
