import datetime
import json
import logging

from django.db.models import Q
from django.http import JsonResponse
from openlink.core.models import Mapping, TaskInfo
from openlink.core.serializers import TaskinfoSerializer
from rest_framework import permissions, viewsets


class TaskinfoViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    queryset = TaskInfo.objects.all()
    serializer_class = TaskinfoSerializer

    def get_queryset(self):
        queryset = TaskInfo.objects.all()
        project_id = None
        max_lenght = None
        delta_minutes = None
        if self.request.query_params.get("project_id"):
            project_id = int(self.request.query_params.get("project_id"))
        if self.request.query_params.get("max_lenght"):
            max_lenght = int(self.request.query_params.get("max_lenght"))
        if self.request.query_params.get("delta_minutes"):
            delta_minutes = int(self.request.query_params.get("delta_minutes"))
        if project_id is not None:
            queryset = queryset.filter(project__id=project_id)
        if delta_minutes is not None:
            creation_time = datetime.datetime.now() - datetime.timedelta(
                minutes=delta_minutes
            )
            queryset = queryset.filter(
                Q(start_date__gte=creation_time)
                | Q(status="queued")
                | Q(status="started")
            )
        if max_lenght is not None:
            queryset = queryset.order_by("-start_date")[:max_lenght]
        return queryset


class MappingViewSet(viewsets.ViewSet):
    def list(self, request):
        mapping_id = self.request.query_params.get("item_id")
        res = mapping_id.strip("][").split(", ")
        all_results = {}
        list_result = []
        for obj in res:
            dict = {}
            try:
                mapping_object = Mapping.objects.filter(
                    foreign_id_obj__id=obj, type="data"
                )
            except Exception as e:
                logging.debug(e)
                continue
            if mapping_object:
                connector = mapping_object[0].tool_id.get_connector(
                    self.request.session["vault_token"]
                )
                has_access = connector.check_file_access(mapping_object[0].object_id)
                dict["id"] = mapping_object[0].id
                dict["access"] = has_access
                list_result.append(dict)

        all_results["results"] = list_result
        data = json.dumps(all_results)
        return JsonResponse(data, safe=False)
