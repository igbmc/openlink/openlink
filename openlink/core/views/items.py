import json
import logging

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Button, Submit
from django.apps import apps
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.views.generic.edit import UpdateView
from guardian.decorators import permission_required_or_403
from openlink.core.connector import Mapper, Publisher
from openlink.core.forms import (
    AssayForm,
    DataForm,
    InvestigationForm,
    ProjectForm,
    StudyForm,
)
from openlink.core.models import (
    Assay,
    Data,
    Investigation,
    Mapping,
    Profile,
    Project,
    Study,
    Tool,
)
from openlink.core.views import projects

logger = logging.getLogger(__name__)


def get_items_id(obj, list_save):
    # Get items structure
    if hasattr(obj, "items"):
        for item in obj.items:
            list_save.append(item)
            get_items_id(item, list_save)
        return list_save


@login_required
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def add_investigation(request, project_id, **kwargs):
    # Display an empty form to add a new investigation

    project = get_object_or_404(Project, pk=project_id)
    if request.method == "GET":
        items = {}
        items["project"] = project
        return render(
            request,
            "new_item/new_item.html",
            {
                "form": InvestigationForm(),
                "project": project,
                "parent": project,
                "item": "investigation",
            },
        )
    elif request.method == "POST":
        form = InvestigationForm(request.POST)
        form.parent_id = project_id
        if not form.is_valid():
            # Display the same empty investigation form if form is not valid
            return render(
                request,
                "new_item/new_item.html",
                {
                    "form": form,
                    "project": project,
                    "parent": project,
                    "item": "investigation",
                },
            )
        profile = get_object_or_404(Profile, user=request.user)

        investigation = Investigation.create(
            form=form, profile=profile, parent_id=project_id
        )

        messages.info(request, "Investigation " + str(investigation.name) + " created")

        return HttpResponseRedirect(reverse("core:projects-details", args=[project_id]))


@login_required
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def add_study(request, investigation_id, project_id):
    # Display an empty form to add a new study  in a given project ID
    investigation = get_object_or_404(Investigation, pk=investigation_id)
    project = get_object_or_404(Project, pk=project_id)
    if request.method == "GET":
        return render(
            request,
            "new_item/new_item.html",
            {
                "form": StudyForm(),
                "project": project,
                "investigation": investigation,
                "parent": investigation,
                "item": "study",
            },
        )
    elif request.method == "POST":
        form = StudyForm(request.POST)
        form.parent_id = investigation_id
        if not form.is_valid():
            # Display the same empty study form if form is not valid
            return render(
                request,
                "new_item/new_item.html",
                {
                    "form": form,
                    "project": project,
                    "investigation": investigation,
                    "parent": investigation,
                    "item": "study",
                },
            )

        profile = get_object_or_404(Profile, user=request.user)
        study = Study.create(form=form, profile=profile, parent_id=investigation_id)

        messages.info(request, "Study " + str(study.name) + " created")

        # Add the new study in the given investigation model instance

        return HttpResponseRedirect(reverse("core:projects-details", args=[project_id]))


@login_required
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def add_assay(request, project_id, investigation_id, study_id):
    # Save the new assay with data from the assay form
    form = AssayForm(request.POST)
    study = get_object_or_404(Study, id=study_id)
    form.parent_id = study_id
    if request.method == "POST":
        if not form.is_valid():
            project = get_object_or_404(Project, pk=project_id)
            investigation = get_object_or_404(Investigation, pk=investigation_id)
            return render(
                request,
                "new_item/new_item.html",
                {
                    "form": form,
                    "study": study,
                    "investigation": investigation,
                    "project": project,
                    "parent": study,
                    "item": "assay",
                },
            )
    else:
        project = get_object_or_404(Project, pk=project_id)
        investigation = get_object_or_404(Investigation, pk=investigation_id)
        study = get_object_or_404(Study, pk=study_id)
        return render(
            request,
            "new_item/new_item.html",
            {
                "form": AssayForm(),
                "study": study,
                "investigation": investigation,
                "project": project,
                "parent": study,
                "item": "assay",
            },
        )

    profile = get_object_or_404(Profile, user=request.user)
    assay = Assay.create(form, profile, study.id)

    messages.info(request, "Assay " + str(assay.name) + " created")

    return HttpResponseRedirect(reverse("core:projects-details", args=[project_id]))


@login_required
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def add_data(request, project_id, investigation_id, assay_id):
    # Save the new data with data from the data form
    form = DataForm(request.POST)
    assay = get_object_or_404(Assay, id=assay_id)
    form.parent_id = assay_id
    if request.method == "POST":
        if not form.is_valid():
            project = get_object_or_404(Project, pk=project_id)
            investigation = get_object_or_404(Investigation, pk=investigation_id)
            return render(
                request,
                "new_item/new_item.html",
                {
                    "form": form,
                    "assay": assay,
                    "investigation": investigation,
                    "project": project,
                    "parent": assay,
                    "item": "data",
                },
            )
    else:
        project = get_object_or_404(Project, pk=project_id)
        investigation = get_object_or_404(Investigation, pk=investigation_id)
        assay = get_object_or_404(Assay, pk=assay_id)
        return render(
            request,
            "new_item/new_item.html",
            {
                "form": DataForm(),
                "assay": assay,
                "investigation": investigation,
                "project": project,
                "parent": assay,
                "item": "data",
            },
        )

    profile = get_object_or_404(Profile, user=request.user)
    data = Data.create(form, profile, assay.id)

    messages.info(request, "Data " + str(data.name) + " created")

    return HttpResponseRedirect(reverse("core:projects-details", args=[project_id]))


@login_required
def get_json_object(request, *args, **kwargs):
    # Build json data to display from json connector
    tool_id = kwargs["tool_id"]
    object_id = kwargs["map_id"]
    data_type = kwargs["data_type"]
    json_data = []
    tool = get_object_or_404(Tool, id=tool_id)
    connector = tool.get_connector(request.session["vault_token"])
    elements_json = connector.get_dir(object_id)
    for element in elements_json:
        if data_type == "data":
            if element.__class__.__name__ == "DataObject":
                json_data.append(
                    {
                        "id": element.id,
                        "parent": object_id,
                        "text": element.name,
                        "icon": "jstree-file",
                    }
                )
            elif element.__class__.__name__ == "ContainerDataObject":
                json_data.append(
                    {
                        "id": element.id,
                        "parent": object_id,
                        "text": element.name,
                        "icon": "jstree-folder",
                    }
                )
        else:
            if element.__class__.__name__ == "DataObject":
                json_data.append(
                    {
                        "id": element.id,
                        "parent": object_id,
                        "text": element.name,
                        "icon": "jstree-file",
                        "state": {"checkbox_disabled": True},
                    }
                )
            elif element.__class__.__name__ == "ContainerDataObject":
                json_data.append(
                    {
                        "id": element.id,
                        "parent": object_id,
                        "text": element.name,
                    }
                )
    json_tree = json.dumps(json_data)

    return render(request, "tools/json_list.html", {"json_tree": json_tree})


def get_plural_form_of_type(type):
    plural_form = {
        "investigation": "investigations",
        "study": "studies",
        "assay": "assays",
        "data": "datas",
    }
    return plural_form[type]


class ItemUpdate(UpdateView):
    # Generic class for item update
    template_name = "item_update/item_update_form.html"

    def __init__(self, *args, **kwargs):
        super(ItemUpdate, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_group_wrapper_class = "row"
        self.helper.label_class = "col-sm-offset-1 col-sm-2"
        self.helper.field_class = "col-md-8"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["name"] = self.object.name
        context["id"] = self.object.id
        context["data_type"] = str(self.object.type)
        obj = apps.get_model("core", str(self.object.type))
        investigation = obj.get_investigation(self.object.id).first()
        context["item"] = get_object_or_404(obj, id=self.object.id)
        context["breadcrumb"] = [
            {"name": context["name"], "type": context["data_type"].lower(), "url": None}
        ]
        id = self.object.id
        obj_instance = obj
        while hasattr(obj_instance, "get_parent"):
            parent = obj_instance.get_parent(id).first()
            investigation = obj_instance.get_investigation(id).first()
            project = obj_instance.get_project(id).first()
            context["breadcrumb"].append(
                {
                    "name": parent.name,
                    "type": obj_instance.get_parent_type().__name__.lower(),
                    "url": parent.get_absolute_url(),
                }
            )
            id = parent.id
            obj_instance = obj_instance.get_parent_type()
        context["breadcrumb"] = context["breadcrumb"][::-1]
        if context["data_type"] != "project":
            project_tools = Tool.objects.filter(project=project)
            logo_tools_available = {}
            for p in project_tools:
                connector = p.get_connector(None)
                if issubclass(connector.__class__, Mapper):
                    list_ressource = connector.get_supported_types()
                    list_map_ressource = connector.get_supported_types(
                        context["item"], p
                    )
                    if (
                        context["data_type"].lower() in list_ressource
                        and context["data_type"].lower() in list_map_ressource
                    ):
                        logo_tools_available[connector.get_color()] = [
                            "True",
                            connector.get_name(),
                        ]
                    elif (
                        context["data_type"].lower() in list_ressource
                        and context["data_type"].lower() not in list_map_ressource
                    ):
                        logo_tools_available[connector.get_color()] = [
                            "False",
                            connector.get_name(),
                        ]
                    else:
                        continue
            context["logo_tools_available"] = logo_tools_available
            if obj == Data:
                assay = get_object_or_404(Assay, datas__id=self.object.id)
                context["assay_linked"] = Mapping.objects.filter(
                    foreign_id_obj__assay__id=assay.id
                ).exists()
            project = obj.get_project(self.object.id).first()
            context["project"] = project
            context["investigation"] = investigation
            self.request.session["reference_data_type"] = self.object.type
            context["request"] = self.request
            context["progress_bar"], context["space_info_list"] = projects.progressbar(
                context["item"], project
            )
            items_mapping = Mapping.objects.filter(foreign_id_obj=context["item"])
            mappings = []
            for item_mapping in items_mapping:
                mapping = []
                connector = item_mapping.connector_class
                if issubclass(connector.__class__, Mapper):
                    supported_types = item_mapping.get_supported_types_plural
                else:
                    supported_types = []
                mapping.append(item_mapping)
                mapping.append(supported_types)
                mappings.append(mapping)
            context["mappings"] = mappings
            context["nb_mapper"] = 0
            context["nb_publisher"] = 0
            for value in items_mapping:
                if (
                    issubclass(
                        Tool.objects.get(id=value.tool_id.id)
                        .get_connector(None)
                        .__class__,
                        Publisher,
                    )
                    is True
                ):
                    context["nb_publisher"] += 1
                else:
                    context["nb_mapper"] += 1
            context["investigation_linked"] = Mapping.objects.filter(
                foreign_id_obj__investigation__id=context["investigation"].id
            ).exists()
            return context
        else:
            context["item"] = get_object_or_404(Project, id=self.object.id)
            return context

    def form_valid(self, form, *args, **kwargs):
        project_id = self.kwargs["project_id"]
        project = get_object_or_404(Project, id=project_id)
        if not self.request.user.has_perm("change_project", project):
            return HttpResponseForbidden()
        item = form.save(commit=False)
        item.date_modified = timezone.now()
        item.save()
        messages.info(
            self.request, item.type.capitalize() + " " + str(item.name) + " updated"
        )
        return HttpResponseRedirect(self.request.get_full_path())

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.helper = FormHelper()
        form.helper.form_group_wrapper_class = "row"
        form.helper.label_class = "col-sm-offset-1 col-sm-2"
        form.helper.field_class = "col-md-8"
        form.helper.add_input(
            Button(
                "back",
                "Back",
                css_class="btn-secondary",
                onClick="javascript:history.go(-1);",
            )
        )
        form.helper.add_input(
            Submit(
                "submit",
                "Update " + self.object.type.capitalize(),
                css_class="btn-primary",
            )
        )
        return form


class ProjectUpdate(ItemUpdate):
    # Generic editing view for a given Project
    model = Project
    form_class = ProjectForm
    template_name = "item_update/project_update_form.html"
    pk_url_kwarg = "project_id"


class InvestigationUpdate(ItemUpdate):
    # Generic editing view for a given Investigation
    model = Investigation
    form_class = InvestigationForm
    pk_url_kwarg = "investigation_id"


class StudyUpdate(ItemUpdate):
    # Generic editing view for a given study
    model = Study
    form_class = StudyForm
    pk_url_kwarg = "study_id"


class AssayUpdate(ItemUpdate):
    # Generic editing view for a given Experience
    model = Assay
    form_class = AssayForm
    pk_url_kwarg = "assay_id"


class DataUpdate(ItemUpdate):
    # Generic editing view for a given Data
    model = Data
    form_class = DataForm
    pk_url_kwarg = "data_id"
