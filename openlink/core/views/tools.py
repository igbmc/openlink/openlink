import logging
import os

import hvac
from crispy_forms.helper import FormHelper
from django.apps import apps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.http import require_GET, require_POST
from django.views.generic.edit import UpdateView
from guardian.decorators import permission_required_or_403
from openlink.core import connector as tools
from openlink.core.connector import Mapper, Publisher
from openlink.core.forms import LinkToolForm, SelectToolForm, ToolForm, ToolProjectForm
from openlink.core.models import Mapping, Profile, Project, Tool, Toolparam

logger = logging.getLogger(__name__)


vault_url = (
    settings.AUTH_VAULT_CONFIG["VAULT_HOSTNAME"]
    + settings.AUTH_VAULT_CONFIG["VAULT_PORT"]
)


@login_required
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def list_tools_project(request, project_id):
    # get list of tools linked to a project
    profile = Profile.objects.get(user=request.user)
    project = get_object_or_404(Project, id=project_id)
    list_tools_project = Tool.objects.filter(project__id=project_id)
    list_tools_linked = profile.tool_list.all()
    return render(
        request,
        "tools/list_tool_project.html",
        {
            "list_tools_project": list_tools_project,
            "project": project,
            "user": request.user,
            "tool_list_linked": list_tools_linked,
        },
    )


@login_required
@require_GET
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def select_connector_project(request, project_id):
    # Get selected connector and return the suited tool form
    project = get_object_or_404(Project, id=project_id)
    if "connector" not in request.GET:
        form = ToolForm()
        return render(
            request,
            "tools/new-choose-connector-project.html",
            {"form": form, "project": project},
        )
    else:
        form = ToolForm(request.GET)
        form.project_id = project_id
        if form.is_valid():
            connector_class = tools.get_connector_class(request.GET["connector"])
            tool_name = request.GET["name"]
            if connector_class is None:
                # TODO: add a message (https://docs.djangoproject.com/fr/3.0/ref/contrib/messages/#using-messages-in-views-and-templates)
                return HttpResponseRedirect(
                    reverse("core:add-tool-project", args=[project_id])
                )
            return render(
                request,
                "tools/add_tool_project.html",
                {
                    "form": connector_class.get_creation_form(),
                    "connector": connector_class.__name__,
                    "tool_name": tool_name,
                    "project": project,
                    "logo": connector_class.get_logo,
                },
            )
        else:
            messages.error(
                request, "unable to select " + request.GET["name"] + " tool connector"
            )
            return render(
                request,
                "tools/new-choose-connector-project.html",
                {"form": form, "project": project},
            )


@login_required
@require_POST
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def add_tool_project(request, connector, project_id):
    # Display tool creation form  and create tool if the form is valid
    connector_class = tools.get_connector_class(connector)
    form = connector_class.get_creation_form(request.POST)
    project = get_object_or_404(Project, id=project_id)
    form.project_id = project_id
    try:
        if not form.is_valid():
            logger.debug(request.POST.get("name"))
            messages.error(
                request, "unable to add " + connector_class.get_name() + " tool"
            )
            return render(
                request,
                "tools/add_tool_project.html",
                {
                    "form": form,
                    "connector": connector,
                    "project": project,
                    "logo": connector_class.get_logo,
                    "tool_name": request.POST.get("name"),
                },
            )
    except Exception as e:
        messages.error(request, str(e))
        return render(
            request,
            "tools/add_tool_project.html",
            {
                "form": form,
                "connector": connector,
                "project": project,
                "logo": connector_class.get_logo,
                "tool_name": request.POST.get("name"),
            },
        )

    if form.is_valid():
        profile = Profile.objects.get(user=request.user)
        # Save new Tool using form entries
        tool = Tool(
            name=request.POST.get("name"),
            connector=connector_class.__name__,
            author=request.user,
            date_created=timezone.now(),
            project=project,
        )
        tool.save()
        tool.user_list.add(profile)
        profile.tool_list.add(tool)
        for param in form.public_data_dict:
            tool_param = Toolparam(
                tool_id=tool,
                key=param,
                value=form.public_data_dict[param],
            )
            tool_param.save()
        if form.private_data_dict:
            private_data = form.private_data_dict
            vault_client = hvac.Client(
                url=vault_url, token=request.session["vault_token"]
            )
            reponse = vault_client.lookup_token()
            path = os.path.join(reponse["data"]["entity_id"], str(tool.id))
            vault_client.secrets.kv.v1.create_or_update_secret(
                mount_point="openlink", path=path, secret=private_data
            )
            if "shared" in request.POST:
                path = os.path.join(
                    "shared", reponse["data"]["entity_id"], str(tool.id)
                )
                vault_client.secrets.kv.v1.create_or_update_secret(
                    mount_point="openlink", path=path, secret=private_data
                )
                profile.tool_list_shared.add(tool)
            vault_client.logout(revoke_token=False)
            del form.private_data_dict, private_data

    # TODO: add a message (https://docs.djangoproject.com/fr/3.0/ref/contrib/messages/#using-messages-in-views-and-templates)
    return HttpResponseRedirect(reverse("core:tools-project", args=[project_id]))


@login_required
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def get_tool_info_project(request, *args, **kwargs):
    # get tool details for a given OpenLink poject
    project = get_object_or_404(Project, id=kwargs["project_id"])
    t_list = Tool.objects.filter(pk=kwargs["tool_id"])
    context = {"tool_list": t_list, "project": project}
    return render(request, "tools/tool_info_project.html", context)


class ToolUpdate(LoginRequiredMixin, UpdateView):
    # Generic editing view for a given Tool
    model = Tool
    form_class = ToolProjectForm
    template_name = "tools/tool_update_form.html"
    pk_url_kwarg = "tool_id"

    def __init__(self, *args, **kwargs):
        super(ToolUpdate, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_group_wrapper_class = "row"
        self.helper.label_class = "col-sm-offset-1 col-sm-2"
        self.helper.field_class = "col-md-8"

    def get_context_data(self, **kwargs):
        project_id = self.kwargs["project_id"]
        context = super().get_context_data(**kwargs)

        context["name"] = self.object.name
        context["id"] = self.object.id
        context["tool"] = self.object
        project = get_object_or_404(Project, id=project_id)
        context["project"] = project
        return context

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.helper = FormHelper()
        form.helper.form_tag = False
        form.helper.disable_csrf = True
        form.helper.form_group_wrapper_class = "row"
        form.helper.label_class = "col-sm-offset-1 col-sm-2"
        form.helper.field_class = "col-md-8"
        form.project_id = self.kwargs["project_id"]
        return form

    def dispatch(self, request, *args, **kwargs):
        project = get_object_or_404(Project, id=self.kwargs["project_id"])
        if not self.request.user.has_perm("change_project", project):
            return HttpResponseForbidden()
        return super(ToolUpdate, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        project_id = self.kwargs["project_id"]
        tool_id = self.kwargs["tool_id"]
        project = get_object_or_404(Project, id=project_id)
        tool = get_object_or_404(Tool, id=tool_id)
        if form.is_valid():
            tool = form.save(commit=False)
            tool.save()
            messages.info(self.request, "Tool " + str(tool.name) + " saved")
            return HttpResponseRedirect(
                reverse("core:tools-project", args=[project.id])
            )
        else:
            return HttpResponseRedirect(
                reverse("core:tools-edit-project", args=[project_id, tool_id])
            )


@login_required
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def delete_tool(request, *args, **kwargs):
    # delete selected tool
    profile = Profile.objects.get(user=request.user)
    obj_id = kwargs["pk"]
    project_id = kwargs["project_id"]
    data_type = "tool"
    obj_to_delete = Tool.objects.get(id=obj_id)
    if obj_to_delete.author != request.user:
        PermissionDenied
    list_user_linked = obj_to_delete.user_list.all()
    if request.method == "POST":
        obj_name = obj_to_delete.name
        # delete credentials in vault
        vault_client = hvac.Client(url=vault_url, token=request.session["vault_token"])
        reponse = vault_client.lookup_token()
        path = os.path.join(reponse["data"]["entity_id"], str(obj_id))
        vault_client.secrets.kv.v1.delete_secret(mount_point="openlink", path=path)
        if obj_to_delete in profile.tool_list_shared.all():
            path = os.path.join("shared", reponse["data"]["entity_id"], str(obj_id))
            vault_client.secrets.kv.v1.delete_secret(mount_point="openlink", path=path)
        messages.info(request, str(obj_name) + " deleted")
        # remove from tool list in profile and delete
        profile.tool_list.remove(obj_to_delete)
        profile.tool_list_shared.remove(obj_to_delete)
        obj_to_delete.delete()
        return HttpResponseRedirect(
            reverse("core:tools-project", kwargs={"project_id": project_id})
        )

    else:
        if data_type == "tool":
            form = LinkToolForm
    return render(
        request,
        "mapping/form_delete.html",
        {
            "form": form,
            "obj": obj_to_delete,
            "data_type": data_type,
            "project_id": project_id,
            "list_user_linked": list_user_linked,
        },
    )


@login_required
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def choose_tool(request, *args, **kwargs):
    # select a tool for the mapping of an OpenLink object
    profile = Profile.objects.get(user=request.user)
    tool_list = []
    if "investigation_id" in kwargs:
        current_inv = kwargs["investigation_id"]
        data_type = kwargs["data_type"]
    else:
        data_type = "investigation"
    current_proj = kwargs["project_id"]
    project = get_object_or_404(Project, id=current_proj)
    exist_tool = 0
    project_tools = Tool.objects.filter(project=project)
    list_tools_linked = profile.tool_list.all()

    pk = kwargs["pk"]
    Model = apps.get_model("core", data_type)
    data_object = get_object_or_404(Model, id=kwargs["pk"])
    tools_not_available = []
    for p in project_tools:
        exist_tool = 1
        connector = p.get_connector(None)
        if issubclass(connector.__class__, Mapper):
            list_all_ressource = connector.get_supported_types()
            list_ressource = connector.get_supported_types(data_object, p)
            if data_type in list_all_ressource:
                if data_type == "investigation":
                    try:
                        mo_list = Mapping.objects.get(
                            foreign_id_obj__investigation__id=pk, tool_id=p
                        )
                    except Mapping.DoesNotExist:
                        mo_list = None
                if data_type == "study":
                    try:
                        mo_list = Mapping.objects.get(
                            foreign_id_obj__study__id=pk, tool_id=p
                        )
                    except Mapping.DoesNotExist:
                        mo_list = None
                elif data_type == "assay":
                    try:
                        mo_list = Mapping.objects.get(
                            foreign_id_obj__assay__id=pk, tool_id=p
                        )
                    except Mapping.DoesNotExist:
                        mo_list = None
                elif data_type == "data":
                    try:
                        mo_list = Mapping.objects.get(
                            foreign_id_obj__data__id=pk, tool_id=p
                        )
                    except Mapping.DoesNotExist:
                        mo_list = None
                if mo_list is None:
                    tool_list.append(p)
            if (
                data_type.lower() in list_all_ressource
                and data_type.lower() not in list_ressource
            ):
                tools_not_available.append(p)
            else:
                continue

    if request.method == "POST":
        host = request.POST["object_name"]
        result = Tool.objects.filter(id=int(host))

        for dp in result:
            tool_id = int(dp.id)
            # tag_tool = dp.tag_tool

            # kwargs['tool'] = str(dp['tool'])
        if "investigation_id" in kwargs:
            return HttpResponseRedirect(
                reverse(
                    "core:choose_non_linked_object_tool",
                    kwargs={
                        "project_id": kwargs["project_id"],
                        "investigation_id": current_inv,
                        "data_type": data_type,
                        "pk": kwargs["pk"],
                        "tool_id": tool_id,
                    },
                )
            )
        else:
            return HttpResponseRedirect(
                reverse(
                    "core:choose_non_linked_object_tool",
                    kwargs={
                        "project_id": kwargs["project_id"],
                        "data_type": data_type,
                        "pk": kwargs["pk"],
                        "tool_id": tool_id,
                    },
                )
            )

    else:
        form = SelectToolForm(instance=tool_list)

    if "investigation_id" in kwargs:
        return render(
            request,
            "tools/form_choose_tool.html",
            {
                "form": form,
                "project": project,
                "exist_tool": exist_tool,
                "all_tool_list": tool_list,
                "current_inv": current_inv,
                "data_type": data_type,
                "pk": pk,
                "list_tools_linked": list_tools_linked,
                "tools_not_available": tools_not_available,
            },
        )
    else:
        return render(
            request,
            "tools/form_choose_tool.html",
            {
                "form": form,
                "project": project,
                "exist_tool": exist_tool,
                "all_tool_list": tool_list,
                "data_type": data_type,
                "pk": pk,
                "list_tools_linked": list_tools_linked,
                "tools_not_available": tools_not_available,
            },
        )


@login_required
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def choose_tool_for_publish(request, *args, **kwargs):
    # Select a tool where to publish data
    profile = Profile.objects.get(user=request.user)
    tool_list = []
    tools_not_available = []
    list_tools_linked = profile.tool_list.all()
    data_type = "investigation"
    current_proj = kwargs["project_id"]
    project = get_object_or_404(Project, id=current_proj)
    project_tools = Tool.objects.filter(project=project)
    pk = kwargs["pk"]

    for p in project_tools:
        connector = p.get_connector(None)
        if issubclass(connector.__class__, Publisher):
            tool_list.append(p)

    if request.method == "POST":
        host = request.POST["object_name"]
        result = Tool.objects.filter(id=int(host))
        for dp in result:
            tool_id = int(dp.id)
            tool_name = dp.name
        return HttpResponseRedirect(
            reverse(
                "core:choose_object_to_publish",
                kwargs={
                    "project_id": kwargs["project_id"],
                    "data_type": data_type,
                    "pk": kwargs["pk"],
                    "tool": tool_name,
                    "tool_id": tool_id,
                },
            )
        )

    else:
        form = SelectToolForm(instance=tool_list)
        return render(
            request,
            "tools/form_choose_tool.html",
            {
                "form": form,
                "project": project,
                "data_type": data_type,
                "pk": pk,
                "function": "publish",
                "all_tool_list": tool_list,
                "list_tools_linked": list_tools_linked,
                "tools_not_available": tools_not_available,
            },
        )


@login_required
@permission_required_or_403("change_project", (Project, "id", "project_id"))
def link_account(request, *args, **kwargs):
    profile = Profile.objects.get(user=request.user)
    # link account to selected tool
    tool_id = kwargs["tool_id"]
    project_id = kwargs["project_id"]

    tool = get_object_or_404(Tool, id=tool_id)

    project = get_object_or_404(Project, id=project_id)
    url = tool.get_public_param(key="url")
    if profile in tool.user_list.all():
        already_linked = True
    else:
        already_linked = False

    connector = tool.connector
    connector_class = tools.get_connector_class(connector)

    form = connector_class.get_addlink_form(request.POST, url=url)
    form.project_id = project_id

    if request.method == "POST":
        if form.is_valid():
            if form.private_data_dict:
                private_data = form.private_data_dict
                vault_client = hvac.Client(
                    url=vault_url, token=request.session["vault_token"]
                )
                reponse = vault_client.lookup_token()
                path = os.path.join(reponse["data"]["entity_id"], str(tool.id))
                vault_client.secrets.kv.v1.create_or_update_secret(
                    mount_point="openlink", path=path, secret=private_data
                )
                if "shared" in request.POST:
                    path = os.path.join(
                        "shared", reponse["data"]["entity_id"], str(tool.id)
                    )
                    vault_client.secrets.kv.v1.create_or_update_secret(
                        mount_point="openlink", path=path, secret=private_data
                    )
                    profile.tool_list_shared.add(tool)
                tool.user_list.add(profile)
                profile.tool_list.add(tool)
                vault_client.logout(revoke_token=False)
                del form.private_data_dict, private_data
            tool.user_list.add(profile)
            return HttpResponseRedirect(
                reverse("core:tools-project", kwargs={"project_id": project_id})
            )
    else:
        form = connector_class.get_addlink_form(data=None, url=url)
        form.project_id = project_id
        return render(
            request,
            "tools/link_account_to_tool.html",
            {
                "form": form,
                "tool": tool,
                "project": project,
                "already_linked": already_linked,
            },
        )
