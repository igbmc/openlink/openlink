import logging
import time
from datetime import datetime
from openlink.core.lib import utils
import django_rq
import hvac
from django.contrib import messages
from django.shortcuts import get_object_or_404
from openlink import settings
from openlink.core.connector import DataObject
from openlink.core.models import Data, Mapping, Profile, TaskInfo, Mappableobject
from rq import get_current_job

logger = logging.getLogger(__name__)


def create_children_mappings(
    request, tool, parent_openlink_object, reference_mapping, container, author
):
    data_type = type(parent_openlink_object)
    data_type_child = data_type.get_child_type()
    project = tool.project
    connector = tool.get_connector(request.session["vault_token"])
    children_data_objects = connector.get_data_objects(
        data_type_child.__name__.lower(), reference_mapping, container
    )
    for child_data_object in children_data_objects:
        if not isinstance(child_data_object, DataObject):
            next
        openlink_object = data_type_child.create_instance(child_data_object, author)
        openlink_object.save()
        messages.info(
            request,
            str(data_type_child.__name__ + " ")
            + str(openlink_object.name)
            + " created ",
        )
        data_type.link_child_to_parent(parent_openlink_object, openlink_object)
        size = 0
        Mapping_openlink_object = Mapping.create_instance(
            openlink_object, child_data_object, tool, author, size
        )
        Mapping_openlink_object.save()
        if type(openlink_object) == Data:
            objects_id = [str(child_data_object.id)]
            target = get_object_or_404(Mappableobject, id=openlink_object.pk)
            user = request.user
            description = "get " + tool.name + " space information"
            utils.add_async_task(
                project,
                user,
                target,
                description,
                connector.get_space_info,
                objects_id,
                Mapping_openlink_object,
            )
        messages.info(
            request, "Mapping " + str(Mapping_openlink_object.name) + " created "
        )
        child_type = data_type_child.get_child_type()
        if child_type is not None:
            create_children_mappings(
                request,
                tool,
                openlink_object,
                reference_mapping,
                child_data_object,
                author,
            )


def add_number_at_end_if_name_exist(object_name, update_object):
    if any(
        d.name == object_name
        for d in update_object.get_parent(update_object.id).first().items
    ):
        i = 1
        name = object_name + " (" + str(i) + ")"
        while any(
            d.name == name
            for d in update_object.get_parent(update_object.id).first().items
        ):
            i += 1
            name = object_name + " (" + str(i) + ")"
        new_name = name
    else:
        new_name = object_name
    return new_name


def add_async_task(project, user, target, description, function, *args):
    # New job Queued for redis default worker
    queue = django_rq.get_queue("default")
    job = queue.enqueue(function, *args)
    # job = function(*args)
    job.meta["description"] = description
    job.save_meta()
    # Add new entry in taskinfo table
    new_taskinfo = TaskInfo()
    new_taskinfo.taskid = job.id
    new_taskinfo.description = description
    new_taskinfo.author = get_object_or_404(Profile, user=user)
    new_taskinfo.project = project
    new_taskinfo.target = target
    new_taskinfo.status = "queued"
    new_taskinfo.save()


def start_async_task():
    time.sleep(1)
    job = get_current_job()
    taskinfo = get_object_or_404(TaskInfo, taskid=job.id)
    taskinfo.status = "started"
    taskinfo.start_date = datetime.now()
    taskinfo.save()
    return job.id


def finish_async_task(jobid, result):
    taskinfo = get_object_or_404(TaskInfo, taskid=jobid)
    taskinfo.result = result
    taskinfo.status = "finished"
    taskinfo.end_date = datetime.now()
    taskinfo.save()


def error_async_task(jobid, e):
    taskinfo = get_object_or_404(TaskInfo, taskid=jobid)
    taskinfo.result = e
    taskinfo.status = "error"
    taskinfo.end_date = datetime.now()
    taskinfo.save()


def get_worker_vault_token():
    vault_url = (
        settings.AUTH_VAULT_CONFIG["VAULT_HOSTNAME"]
        + settings.AUTH_VAULT_CONFIG["VAULT_PORT"]
    )
    try:
        vault_client = hvac.Client(vault_url)
        vault_client.auth.approle.login(
            role_id=settings.VAULT_ROLE_ID,
            secret_id=settings.VAULT_SECRET_ID,
        )
        vault_client.kv.default_kv_version = 1
        response = vault_client.lookup_token()
        token = response["data"]["id"]
        return token
    except Exception as e:
        raise e


def get_user_from_inspect():
    import inspect

    for frame_record in inspect.stack():
        if frame_record[3] == "get_response":
            request = frame_record[0].f_locals["request"]
            break
        else:
            request = None
    return request.user
