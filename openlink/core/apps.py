from django.apps import AppConfig


class OpenlinkAppConfig(AppConfig):
    name = "core"
