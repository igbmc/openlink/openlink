from django.apps import AppConfig


class OpenlinkSeafileConfig(AppConfig):
    name = "openlink_seafile"
