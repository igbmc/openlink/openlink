import io
import logging
import time
import zipfile
from openlink.core.lib import utils
import openlink.core.connector
import requests
from django import forms
from openlink.core.connector import (
    AuthentificationError,
    ContainerDataObject,
    ContainerObject,
    DataObject,
    Mapper,
    ToolConnector,
    ToolForm,
    ToolUnreachableError,
    defaultError,
    permissionError,
)

logger = logging.getLogger(__name__)


class SeafileForm(ToolForm):
    url = forms.URLField(
        label="SeaFile URL",
        help_text="example: https://url_seafile.com",
        max_length=100,
        required=True,
    )
    login = forms.CharField(label="Login", max_length=100, required=True)
    password = forms.CharField(
        widget=forms.PasswordInput, label="Password", max_length=100, required=True
    )
    shared = forms.BooleanField(
        label="Allows asynchronous tasks",
        help_text="This option allows openlink to access your login and password while not connected to run multiple asynchronous tasks",
        required=False,
    )
    public_data_dict = {}
    private_data_dict = {}

    def clean(self):
        cleaned_data = super().clean()
        url = cleaned_data.get("url")
        login = cleaned_data.get("login")
        password = cleaned_data.get("password")

        auth_url = url + "/api2/auth-token/"
        auth_url = auth_url.replace("//api", "/api")
        try:
            token = SeafileConnector.get_api_token(auth_url, login, password)
            cleaned_data["token"] = token
        except AuthentificationError as e:
            self.add_error("password", str(e))
            self.add_error("login", str(e))
        except requests.exceptions.ConnectionError:
            self.add_error("url", "invalid seafile URL ")
        cleaned_data.pop("login")
        cleaned_data.pop("password")

        public_data_list = ("url", "")
        private_data_list = ("token", "")

        self.public_data_dict = {
            k: cleaned_data[k] for k in public_data_list if k in cleaned_data
        }
        self.private_data_dict = {
            k: cleaned_data[k] for k in private_data_list if k in cleaned_data
        }

        return cleaned_data


class SeafileEditCredentials(ToolForm):
    url = forms.URLField()
    login = forms.CharField(label="Login", max_length=100, required=True)
    password = forms.CharField(
        widget=forms.PasswordInput, label="Password", max_length=100, required=True
    )
    shared = forms.BooleanField(
        label="Allows asynchronous tasks",
        help_text="This option allows openlink to access your login and password while not connected to run multiple asynchronous tasks",
        required=False,
    )
    private_data_dict = {}

    def __init__(self, data, *args, **kwargs):
        url = kwargs["url"]
        kwargs.pop("url")
        super(SeafileEditCredentials, self).__init__(data)
        self.fields["url"] = forms.URLField(
            label="Seafile URL",
            max_length=100,
            required=True,
            initial=url,
            disabled=True,
        )

    def clean(self):
        cleaned_data = super().clean()
        url = cleaned_data.get("url")
        login = cleaned_data.get("login")
        password = cleaned_data.get("password")

        auth_url = url + "/api2/auth-token/"
        auth_url = auth_url.replace("//api", "/api")
        try:
            token = SeafileConnector.get_api_token(auth_url, login, password)
            cleaned_data["token"] = token
        except AuthentificationError as e:
            self.add_error("password", str(e))
            self.add_error("login", str(e))
        except requests.exceptions.ConnectionError:
            self.add_error("url", "invalid seafile URL ")
        cleaned_data.pop("login")
        cleaned_data.pop("password")
        private_data_list = ("token", "")
        self.private_data_dict = {
            k: cleaned_data[k] for k in private_data_list if k in cleaned_data
        }

        return cleaned_data


class SeafileConnector(Mapper):
    def __init__(self, tool, token, user_vault_id=None):
        self.url = tool.get_public_param("url")
        self.token = tool.get_private_param("token", token, user_vault_id=user_vault_id)
        self.tool = tool

    @classmethod
    def get_name(cls):
        return "Seafile"

    @classmethod
    def get_data_structure(cls):
        return openlink.core.connector.TREE_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_addlink_form(cls, data, *args, **kwargs):
        return SeafileEditCredentials(data, *args, **kwargs)

    @classmethod
    def get_supported_types(cls, data_object=None, tool=None):
        list_supported_types = {
            "assay": ["directory"],
            "data": ["file", "directory"],
        }
        return list_supported_types

    @classmethod
    def get_plural_form_of_type(cls, type):
        plural_form = {"file": "files", "directory": "directories"}
        return plural_form[type]

    @classmethod
    def get_creation_form(cls, data=None):
        return SeafileForm(data)

    @classmethod
    def get_logo(cls):
        logo = "images/logo-seafile.png"
        return logo

    @classmethod
    def get_color(cls):
        color = "#F1D583"
        return color

    @classmethod
    def has_mapping_options(cls):
        return False

    @classmethod
    def check_status_code(cls, code):
        code = code
        logger.debug(code)
        if code != requests.codes.ok:
            if code == 400:
                raise AuthentificationError(cls, "login or password")
            elif code == (500 or 429):
                raise ToolUnreachableError(cls)
            elif code == 403:
                raise permissionError(cls)
            else:
                raise defaultError(cls)

    @classmethod
    def get_api_token(cls, url, login, password):
        info = {"username": login, "password": password}
        r = requests.post(url, data=info)
        cls.check_status_code(r.status_code)
        token = r.json()["token"]
        return token

    def get_api_url(self):
        url_api = "%s/api/v2.1/" % (self.url)
        url_api = url_api.replace("//api", "/api")
        return url_api

    def get_api_url2(self):
        url_api = "%s/api2/" % (self.url)
        url_api = url_api.replace("//api", "/api")
        return url_api

    def get_url_root(self):
        url_root = "%s/seafhttp/" % (self.url)
        url_root = url_root.replace("//seafhttp", "/seafhttp")
        return url_root

    def get_url_link_to_an_object(self, obj_type, obj_id):
        # return a url to display for current user
        WEB_DISPLAY = "/library/"
        if obj_type == "assay":
            url = self.url + WEB_DISPLAY + obj_id
        elif obj_type == "data":
            if "/file/" in obj_id:
                WEB_DISPLAY = "/lib/"
            url = self.url + WEB_DISPLAY + obj_id
        else:
            url = self.url + WEB_DISPLAY + obj_id
        url = url.replace("//library", "/library").replace("//lib", "/lib")
        return url

    def get_space_info(self, objects_id, mapping_object=None):
        if mapping_object:
            jobid = utils.start_async_task()
        try:
            size_str = 0
            type_d = ""
            for object_path in objects_id:
                if "/" in object_path:
                    project = object_path.split("/")[0]
                    name = object_path.split("/")[-1]
                    path_id = object_path.split("/", 2)[2].split("/")[:-1]
                    path_dir = "/".join(path_id)
                    if object_path.split("/")[1] == "file":
                        type_d = "file"
                        url = "{url_api}repos/{id}/dir/?p=/{dir_id}".format(
                            url_api=self.get_api_url(), id=project, dir_id=path_dir
                        )
                    else:
                        type_d = "dir"
                        url = "{url_api}repos/{id}/dir/?t=f&recursive=1&p=/{dir_id}".format(
                            url_api=self.get_api_url(), id=project, dir_id=path_dir
                        )
                else:
                    url = "{url_api}repos/{id}/".format(
                        url_api=self.get_api_url(), id=object_path
                    )
                headers = {"Authorization": "Token " + str(self.token)}
                try:
                    response = requests.get(url, headers=headers)
                    logger.debug(SeafileConnector.check_status_code(response.status_code))
                except Exception:
                    size_str = -1
                    size = int(size_str)
                    return size
                elements = response.json()
                for element in elements["dirent_list"]:
                    if type_d == "file":
                        if element["type"] == "file" and element["name"] == name:
                            size_str += element["size"]
                    elif type_d == "dir":
                        if element["type"] == "file":
                            size_str += element["size"]
            size = int(size_str)
            if mapping_object:
                mapping_object.size = size
                mapping_object.save()
                utils.finish_async_task(jobid, size)
            else:
                return size
        except Exception as e:
            utils.error_async_task(jobid)
            return e

    def get_investigation(self, object_id):
        logger.debug("get inv seaf")
        url = "{url_api}repos/{id}/".format(url_api=self.get_api_url(), id=object_id)
        headers = {"Authorization": "Token " + self.token}
        try:
            response = requests.get(url, headers=headers)
            SeafileConnector.check_status_code(response.status_code)
            library = response.json()
        except Exception as e:
            raise e
        obj_to_link = DataObject(library["repo_id"], library["repo_name"])
        return obj_to_link

    def get_study(self, object_id):
        return None

    def get_assay(self, object_id):
        name = object_id.rsplit("/")[-1]
        obj_to_link = ContainerDataObject(object_id, name, "directory")
        return obj_to_link

    def get_data(self, object_id):
        name = object_id.rsplit("/")[-1]
        obj_to_link = DataObject(object_id, name, "file")
        return obj_to_link

    def get_investigations(self):
        logger.debug("get invs seaf")
        # List all libraries
        logger.debug("ins seaf")
        url = "{url_api}repos/".format(url_api=self.get_api_url())
        logging.debug(url)
        headers = {"Authorization": "Token " + self.token}
        try:
            response = requests.get(url, headers=headers)
            SeafileConnector.check_status_code(response.status_code)
            all_libraries = response.json()
        except Exception as e:
            raise e
        projects = []
        for project in all_libraries["repos"]:
            projects.append(
                ContainerObject(project["repo_id"], project["repo_name"], "library")
            )
        return projects

    def get_studies(self, reference_mapping, container):
        return []

    def get_assays(self, reference_mapping, container):
        logger.debug("get asys seaf")
        folders = []
        if container is None:
            folders = self.get_investigations()
        else:
            projects = []
            projects.append(container.id)
            for project in projects:
                url = "{url_api}repos/{id}/".format(
                    url_api=self.get_api_url(), id=project
                )
                headers = {"Authorization": "Token " + self.token}
                try:
                    response = requests.get(url, headers=headers)
                    SeafileConnector.check_status_code(response.status_code)
                except Exception as e:
                    raise e
                library = response.json()
                repo_name = library["repo_name"]
                # List directory entries
                url = "{url_api}repos/{id}/dir/?p=/".format(
                    url_api=self.get_api_url(), id=project
                )
                try:
                    response = requests.get(url, headers=headers)
                    SeafileConnector.check_status_code(response.status_code)
                except Exception as e:
                    raise e
                elements_json = response.json()
                for element in elements_json["dirent_list"]:
                    folders.append(
                        ContainerDataObject(
                            project + "/" + repo_name + "/" + element["name"],
                            element["name"],
                            element["type"],
                        )
                    )
        return folders

    def get_datas(self, reference_mapping, container):
        logger.debug("get datas seaf")
        datas = []
        if container is None:
            datas = self.get_investigations()
        else:
            if "/" in str(container.id):
                path_id = str(container.id).split("/", 2)[2]
                project_nb = str(container.id).split("/")[0]
                url = "{url_api}repos/{id}/dir/?p=/{dir_id}".format(
                    url_api=self.get_api_url(), id=project_nb, dir_id=path_id
                )
            else:
                path_id = ""
                project_nb = container.id
                url = "{url_api}repos/{id}/dir/?p=/".format(
                    url_api=self.get_api_url(), id=container.id
                )
            headers = {"Authorization": "Token " + self.token}
            try:
                response = requests.get(url, headers=headers)
                SeafileConnector.check_status_code(response.status_code)
            except Exception as e:
                raise e
            elements_json = response.json()
            for element in elements_json["dirent_list"]:
                if element["type"] == "file":
                    datas.append(
                        DataObject(
                            str(
                                project_nb
                                + "/"
                                + "file"
                                + "/"
                                + path_id
                                + "/"
                                + element["name"]
                            ).replace("//", "/"),
                            element["name"],
                            element["type"],
                        )
                    )
                else:
                    datas.append(
                        ContainerDataObject(
                            str(container.id + "/" + element["name"]).replace(
                                "//", "/"
                            ),
                            element["name"],
                            element["type"],
                        )
                    )
        return datas

    def get_dir(self, project_id):
        logger.debug("get dir seaf")
        object_json = []
        headers = {"Authorization": "Token " + self.token}
        logger.debug(project_id)
        if "/" in project_id:
            logger.debug("if /")
            project_name = project_id.split("/")[0]
            path_id = project_id.split("/", 2)[2]
            url = "{url_api}repos/{id}/dir/?p=/{dir_id}".format(
                url_api=self.get_api_url(), id=project_name, dir_id=path_id
            )
        else:
            url_repo = "{url_api}repos/{id}/".format(
                url_api=self.get_api_url(), id=project_id
            )
            try:
                response = requests.get(url_repo, headers=headers)
                SeafileConnector.check_status_code(response.status_code)
            except Exception as e:
                raise e
            project_json = response.json()
            repo_name = project_json["repo_name"]
            url = "{url_api}repos/{id}/dir/?p=/".format(
                url_api=self.get_api_url(), id=project_id
            )
        try:
            response = requests.get(url, headers=headers)
            SeafileConnector.check_status_code(response.status_code)
        except Exception as e:
            raise e
        elements_json = response.json()
        for element in elements_json["dirent_list"]:
            if "/" in project_id:
                if element["type"] == "file":
                    object_json.append(
                        DataObject(
                            str(
                                project_name
                                + "/"
                                + "file"
                                + "/"
                                + path_id
                                + "/"
                                + element["name"]
                            ).replace("//", "/"),
                            element["name"],
                            element["type"],
                        )
                    )
                else:
                    object_json.append(
                        ContainerDataObject(
                            str(project_id + "/" + element["name"]).replace("//", "/"),
                            element["name"],
                            element["type"],
                        )
                    )
            else:
                if element["type"] == "file":
                    object_json.append(
                        DataObject(
                            str(
                                project_id + "/" + "file" + "/" + "/" + element["name"]
                            ).replace("//", "/"),
                            element["name"],
                            element["type"],
                        )
                    )
                else:
                    object_json.append(
                        ContainerDataObject(
                            str(
                                project_id + "/" + repo_name + "/" + element["name"]
                            ).replace("//", "/"),
                            element["name"],
                            element["type"],
                        )
                    )

        return object_json

    def get_data_objects(self, data_type, reference_mapping=None, container=None):
        logger.debug("get obj seaf")
        get_objects = []
        if data_type == "investigation":
            get_objects = self.get_investigations()
        elif data_type == "study":
            get_objects = self.get_studies(reference_mapping, container)
        elif data_type == "assay":
            get_objects = self.get_assays(reference_mapping, container)
        elif data_type == "data":
            get_objects = self.get_datas(reference_mapping, container)
        return get_objects

    def get_data_object(self, data_type, object_id):
        logger.debug("get objs seaf")
        if data_type == "investigation":
            get_object = self.get_investigation(object_id)
        elif data_type == "study":
            get_object = self.get_study(object_id)
        elif data_type == "assay":
            get_object = self.get_assay(object_id)
        elif data_type == "data":
            get_object = self.get_data(object_id)
        return get_object

    def download(self, object_id, path):
        headers = {"Authorization": "Token " + self.token}
        if "/file" in object_id:
            object_id_m = object_id.split("/")[0]
            directory_id = object_id.rsplit("%s/file/" % object_id_m)[1]
            url = "{url_api}repos/{id}/file/?p=/{file}".format(
                url_api=self.get_api_url2(), id=object_id_m, file=directory_id
            )

            try:
                response = requests.get(url, headers=headers)
                SeafileConnector.check_status_code(response.status_code)
            except Exception as e:
                raise e
            elements = response.json()
            try:
                r = requests.get(elements, headers=headers)
                SeafileConnector.check_status_code(r.status_code)
            except Exception as e:
                raise e
            open("%s/%s" % (path, object_id.split("/")[-1]), "wb").write(r.content)
        else:
            object_id_m = object_id.split("/")[0]
            directory_id = object_id.rsplit("%s/" % object_id_m)[1]
            parent_dir = directory_id.rsplit("/", 1)[0]
            parent_dir = parent_dir.split("/", 1)[1]
            dir = object_id.split("/")[-1]
            url = "{url_api}repos/{id}/zip-task/?parent_dir={parent}&dirents={dir}".format(
                url_api=self.get_api_url(), id=object_id_m, parent=parent_dir, dir=dir
            )
            try:
                response = requests.get(url, headers=headers)
                SeafileConnector.check_status_code(response.status_code)
            except Exception as e:
                logger.debug(e)
                raise e
            logger.debug(response.text)
            elementsdict = response.json()
            ziptoken = elementsdict["zip_token"]
            url2 = "{url_api}query-zip-progress/?token={token}".format(
                url_api=self.get_api_url(), token=ziptoken
            )
            download = False
            while not download:
                try:
                    response = requests.get(url2, headers=headers)
                    SeafileConnector.check_status_code(response.status_code)
                except Exception as e:
                    raise e
                elementsdict = response.json()
                if elementsdict["zipped"] == elementsdict["total"]:
                    url3 = "{url_root}zip/{token}".format(
                        url_root=self.get_url_root(), token=ziptoken
                    )
                    try:
                        r = requests.get(url3, headers=headers)
                        SeafileConnector.check_status_code(r.status_code)
                    except Exception as e:
                        raise e
                    z = zipfile.ZipFile(io.BytesIO(r.content))
                    z.extractall(path)
                    download = True
                else:
                    time.sleep(0.125)

    def check_file_access(self, object_id):
        headers = {"Authorization": "Token " + self.token}
        if "/file" in object_id:
            object_id_m = object_id.split("/")[0]
            directory_id = object_id.rsplit("%s/file/" % object_id_m)[1]
            url = "{url_api}repos/{id}/file/?p=/{file}".format(
                url_api=self.get_api_url2(), id=object_id_m, file=directory_id
            )
            try:
                response = requests.get(url, headers=headers)
                SeafileConnector.check_status_code(response.status_code)
            except Exception as e:
                logger.debug(e)
            elements = response.json()
            try:
                r = requests.get(elements, headers=headers)
                SeafileConnector.check_status_code(r.status_code)
                access = True
            except Exception as e:
                access = False
                logger.debug(e)
        else:
            object_id_m = object_id.split("/")[0]
            directory_id = object_id.rsplit("%s/" % object_id_m)[1]
            parent_dir = directory_id.rsplit("/", 1)[0]
            parent_dir = parent_dir.split("/", 1)[1]
            dir = object_id.split("/")[-1]
            url = "{url_api}repos/{id}/zip-task/?parent_dir={parent}&dirents={dir}".format(
                url_api=self.get_api_url(), id=object_id_m, parent=parent_dir, dir=dir
            )
            try:
                response = requests.get(url, headers=headers)
                SeafileConnector.check_status_code(response.status_code)
                access = True
            except Exception as e:
                access = False
                logger.debug(e)
        return access

    def get_description(self):
        return ""

    def get_information(self, type, id):
        headers = {"Authorization": "Token " + self.token}
        info = {}
        url2 = None
        if "/file/" in id:
            project_id_m = id.split("/")[0]
            directory_id = id.rsplit("%s/file/" % project_id_m)[1]
            url = "{url_api}repos/{id}/file/detail/?p=/{file}".format(
                url_api=self.get_api_url2(), id=project_id_m, file=directory_id
            )
        elif "/" in id:
            project_id_m = id.split("/")[0]
            directory_id = id.split("/", 2)[2]
            url = "{url_api}repos/{id}/dir/detail/?path={path}".format(
                url_api=self.get_api_url(), id=project_id_m, path=directory_id
            )
            url2 = "{url_api}repos/{id}/dir/shared_items/?p=/&share_type=".format(
                url_api=self.get_api_url2(), id=project_id_m
            )
        else:
            url = "{url_api}repos/{id}".format(url_api=self.get_api_url2(), id=id)
            url2 = "{url_api}repos/{id}/dir/shared_items/?p=/&share_type=".format(
                url_api=self.get_api_url2(), id=id
            )
        try:
            response = requests.get(url, headers=headers)
            SeafileConnector.check_status_code(response.status_code)
        except Exception as e:
            raise e
        elementsdict = response.json()
        info["title"] = elementsdict["name"]
        if url2 is not None:
            url2 += "user"
            response = requests.get(url2, headers=headers)
            elementsdict = response.json()

            info["user"] = []
            for element in elementsdict:
                info["user"].append(element["user_info"]["nickname"])

            url2 = url2.replace("user", "group")
            response = requests.get(url2, headers=headers)
            elementsdict = response.json()
            for element in elementsdict:
                groupurl = "{url_api}groups/{id}/members".format(
                    url_api=self.get_api_url(), id=element["group_info"]["id"]
                )
                response = requests.get(groupurl, headers=headers)
                elementsdict = response.json()
                for groupuser in elementsdict:
                    info["user"].append(groupuser["name"])
        return info


ToolConnector.register(SeafileConnector)
