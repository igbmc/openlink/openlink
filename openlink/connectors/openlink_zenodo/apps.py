from django.apps import AppConfig


class OpenlinkZenodoConfig(AppConfig):
    name = "openlink_zenodo"
