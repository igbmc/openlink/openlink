# from django.shortcuts import render
import json
import logging
import os

import openlink.core.connector
import requests
from django import forms
from django.dispatch import receiver
from openlink.core.connector import (
    AuthentificationError,
    ContainerDataObject,
    DataObject,
    Mapper,
    ToolConnector,
    ToolForm,
    defaultError,
)
from openlink.core.lib import utils
from openlink.core.models import Assay, Data, Mapping, MappingParam, Profile, Tool
from openlink.core.signal import delete_signal, new_mapping_signal

logger = logging.getLogger(__name__)

message = """<h5>How to get a new Labguru token?</h5>
    <ol>
        <li>First, check the current workspace you are in</li>
        <li>Then, go to your labguru Profile</li>
        <li>Click on Labguru's Upfolder</li>
        <li>Finally, click on the blue rectangle that says "Click here to get a token by email"</li>
        <li>The new Labguru Token will appears in your inbox</li>
    </ol>
    """

"""
Creation form
"""


class LabGuruForm(ToolForm):
    url = forms.URLField(
        label="LabGuru URL",
        help_text="example: https://url_labguru.com",
        max_length=100,
        required=True,
    )
    private_token = forms.CharField(
        label="Private token", help_text=message, max_length=100, required=True
    )
    shared = forms.BooleanField(
        label="Allows asynchronous tasks",
        help_text="This option allows openlink to access your login and password while not connected to run multiple asynchronous tasks",
        required=False,
    )
    public_data_dict = {}
    private_data_dict = {}

    def clean(self):
        cleaned_data = super().clean()
        url = cleaned_data.get("url")
        private_token = cleaned_data.get("private_token")
        cleaned_data["token"] = cleaned_data.pop("private_token")
        test_url = "%s/api/v1/admin/members.json?token=" % (url)
        test_url = test_url.replace("//api", "/api")
        try:
            LabGuruConnector.test_private_token(test_url, private_token)
        except AuthentificationError as e:
            self.add_error("private_token", str(e))
        except (requests.exceptions.ConnectionError, json.JSONDecodeError):
            self.add_error("url", "Invalid LabGuru URL")
        except Exception as e:
            raise Exception(str(e))
        public_data_list = ("url", "")
        private_data_list = ("token", "")

        self.public_data_dict = {
            k: cleaned_data[k] for k in public_data_list if k in cleaned_data
        }
        self.private_data_dict = {
            k: cleaned_data[k] for k in private_data_list if k in cleaned_data
        }

        return cleaned_data


"""
Form to change credentials.
Should only return private param
"""


class LabGuruEditCredentials(ToolForm):
    url = forms.URLField()
    private_token = forms.CharField(
        label="Private token", help_text=message, max_length=100, required=True
    )
    shared = forms.BooleanField(
        label="Allows asynchronous tasks",
        help_text="This option allows openlink to access your login and password while not connected to run multiple asynchronous tasks",
        required=False,
    )
    private_data_dict = {}

    def __init__(self, data, *args, **kwargs):
        url = kwargs["url"]
        kwargs.pop("url")
        super(LabGuruEditCredentials, self).__init__(data)
        self.fields["url"] = forms.URLField(
            label="Labguru URL",
            max_length=100,
            required=True,
            initial=url,
            disabled=True,
        )

    def clean(self):
        cleaned_data = super().clean()
        url = cleaned_data.get("url")
        private_token = cleaned_data.get("private_token")
        cleaned_data["token"] = cleaned_data.pop("private_token")
        test_url = "%s/api/v1/admin/members.json?token=" % (url)
        test_url = test_url.replace("//api", "/api")
        try:
            LabGuruConnector.test_private_token(test_url, private_token)
        except AuthentificationError as e:
            self.add_error("private_token", str(e))
        except (requests.exceptions.ConnectionError, json.JSONDecodeError):
            self.add_error("url", "Invalid LabGuru URL")
        except Exception as e:
            raise Exception(str(e))
        private_data_list = ("token", "")
        self.private_data_dict = {
            k: cleaned_data[k] for k in private_data_list if k in cleaned_data
        }

        return cleaned_data


class LabGuruConnector(Mapper):
    def __init__(self, tool, token, user_vault_id=None):
        self.url = tool.get_public_param("url")
        self.token = tool.get_private_param("token", token, user_vault_id)
        self.tool = tool

    @classmethod
    def get_name(cls):
        return "LabGuru"

    @classmethod
    def get_data_structure(cls):
        return openlink.core.connector.LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_addlink_form(cls, data, *args, **kwargs):
        return LabGuruEditCredentials(data, *args, **kwargs)

    @classmethod
    def get_supported_types(cls, data_object=None, tool=None):
        list_supported_types = {
            "investigation": ["project"],
            "study": ["folder"],
            "assay": ["experiment"],
        }
        if data_object:
            parent = data_object.get_parent(data_object.id)
            try:
                Mapping.objects.get(foreign_id_obj=parent[0], tool_id=tool)
            except Mapping.DoesNotExist:
                if data_object.__class__.__name__.lower() != "investigation":
                    list_supported_types.pop(
                        data_object.__class__.__name__.lower(), None
                    )
        return list_supported_types

    def get_plural_form_of_type(cls, type):
        plural_form = {
            "project": "projects",
            "folder": "folders",
            "experiment": "experiments",
        }
        return plural_form[type]

    @classmethod
    def get_creation_form(cls, data=None):
        return LabGuruForm(data)

    @classmethod
    def get_logo(cls):
        logo = "images/logo-labguru.webp"
        return logo

    @classmethod
    def get_color(cls):
        color = "#CEA4F7"
        return color

    @classmethod
    def check_status_code(cls, r):
        if r != requests.codes.ok:
            if r == 401:
                raise AuthentificationError(cls, "token")
            else:
                raise defaultError(cls)

    @classmethod
    def test_private_token(cls, url, private_token):
        full_url = url + private_token
        r = requests.get(full_url)
        cls.check_status_code(r.status_code)

    @classmethod
    def has_mapping_options(cls):
        return True

    def get_fields_option_mapping(cls, fields):
        fields["labguru_synchronize_data_with_experiment"] = forms.BooleanField(
            label="Link future data mapping in Labguru experiment?",
            widget=forms.CheckboxInput(),
            required=False,
        )
        return ["labguru_synchronize_data_with_experiment"]

    def get_api_url(self):
        WEB_HOST = "v1/"
        url_api = "%s/api/%s" % (self.url, WEB_HOST)
        url_api = url_api.replace("//api", "/api")
        return url_api

    def get_url_link_to_an_object(self, obj_type, obj_id):
        # return a url to display for current user
        WEB_DISPLAY = "/knowledge/"
        if obj_type == "investigation":
            obj_type = "projects"
        elif obj_type == "study":
            obj_type = "milestones"
        elif obj_type == "assay":
            obj_type = "experiments"
        elif obj_type == "data":
            obj_type = "datas"
            WEB_DISPLAY = ""
        url = self.url + WEB_DISPLAY + obj_type + "/" + obj_id
        return url

    def get_space_info(self, objects_id):
        space = "None"
        return space

    def get_investigation(self, obj_id):
        url = "{url_api}/projects/{obj_id}.json?token={token}".format(
            url_api=self.get_api_url(), obj_id=obj_id, token=self.token
        )
        project = requests.get(url)
        try:
            LabGuruConnector.check_status_code(project.status_code)
        except Exception as e:
            return e
        project_json = project.json()
        # obj_to_link = LabguruObject(project_json)
        obj_to_link = ContainerDataObject(
            project_json["id"],
            project_json["name"],
            "project",
            project_json["description"],
        )
        {
            "id": project_json["id"],
            "name": project_json["name"],
            "description": project_json["description"],
        }
        return obj_to_link

    def get_study(self, obj_id):
        url = "{url_api}/milestones/{obj_id}.json?token={token}".format(
            url_api=self.get_api_url(), obj_id=obj_id, token=self.token
        )
        folder = requests.get(url)
        try:
            LabGuruConnector.check_status_code(folder.status_code)
        except Exception as e:
            raise e
        folder_json = folder.json()
        obj_to_link = ContainerDataObject(
            folder_json["id"], folder_json["name"], "folder", folder_json["experiments"]
        )
        #     "id": folder_json["id"],
        #     "name": folder_json["name"],
        #     "description": folder_json["description"],
        #     "experiments": folder_json["experiments"],
        #     "folder": folder_json["milestones"],
        # }

        return obj_to_link

    def get_assay(self, obj_id):
        url = "{url_api}/experiments/{obj_id}.json?token={token}".format(
            url_api=self.get_api_url(), obj_id=obj_id, token=self.token
        )
        experiment = requests.get(url)
        try:
            LabGuruConnector.check_status_code(experiment.status_code)
        except Exception as e:
            raise e
        experiment_json = experiment.json()
        obj_to_link = DataObject(
            experiment_json["id"],
            experiment_json["name"],
            "experiment",
            experiment_json["description"],
        )

        #     "id": experiment_json["id"],
        #     "name": experiment_json["name"],
        #     "description": experiment_json["description"],
        #     "folder": experiment_json["milestone"],
        #     "uuid": experiment_json["uuid"],
        # }
        return obj_to_link

    def get_data(self, obj_id):
        url = "{url_api}/datas/{obj_id}.json?token={token}".format(
            url_api=self.get_api_url(), obj_id=obj_id, token=self.token
        )
        data = requests.get(url)
        try:
            LabGuruConnector.check_status_code(data.status_code)
        except Exception as e:
            raise e
        data_json = data.json()
        obj_to_link = DataObject(data_json["id"], data_json["name"], "data")
        return obj_to_link

    def get_investigations(self):
        url = "{url_api}/projects.json?token={token}".format(
            url_api=self.get_api_url(), token=self.token
        )
        all_projects = requests.get(url)
        try:
            LabGuruConnector.check_status_code(all_projects.status_code)
        except Exception as e:
            raise e
        projects = []
        for project in all_projects.json():
            projects.append(
                ContainerDataObject(project["id"], project["name"], "project")
            )
        return projects

    def get_studies(self, reference_mapping, container):
        folders = []
        url = "{url_api}/milestones.json?token={token}&age=1&kendo=true&filter[logic]=and&filter[filters][0][field]=project_id&filter[filters][0][operator]=eq&filter[filters][0][value]={project_id}".format(
            url_api=self.get_api_url(), token=self.token, project_id=container.id
        )
        all_folders = requests.get(url)
        try:
            LabGuruConnector.check_status_code(all_folders.status_code)
        except Exception as e:
            raise e
        for folder in all_folders.json():
            folders.append(
                ContainerDataObject(folder["id"], folder["name"], "folder")
                # {
                #     "id": folder["id"],
                #     "name": folder["name"],
                #     "type_object": "folder",
                #     "tool_name": self.tool.name,
                #     "tool": self.tool,
                # }
            )
        return folders

    def get_assays(self, reference_mapping, container):
        experiments = []
        if container is not None:
            url = "{url_api}/experiments.json?token={token}&age=1&kendo=true&filter[logic]=and&filter[filters][0][field]=milestone_id&filter[filters][0][operator]=eq&filter[filters][0][value]={container_id}".format(
                url_api=self.get_api_url(), token=self.token, container_id=container.id
            )
        elif reference_mapping is not None:
            url = "{url_api}/experiments.json?token={token}&age=1&kendo=true&filter[logic]=and&filter[filters][0][field]=project_id&filter[filters][0][operator]=eq&filter[filters][0][value]={project_id}".format(
                url_api=self.get_api_url(),
                token=self.token,
                project_id=reference_mapping.id,
            )
        all_experiments = requests.get(url)
        try:
            LabGuruConnector.check_status_code(all_experiments.status_code)
        except Exception as e:
            raise e
        for experiment in all_experiments.json():
            experiments.append(
                DataObject(experiment["id"], experiment["name"], "experiment")
                # {
                #     "id": experiment["id"],
                #     "name": experiment["name"],
                #     "type_object": "experiment",
                #     "tool_name": self.tool.name,
                #     "tool": self.tool,
                # }
            )
        return experiments

    def get_datas(self, reference_mapping, container):
        return []

    def get_data_objects(self, data_type, reference_mapping=None, container=None):
        if data_type == "investigation":
            get_objects = self.get_investigations()
        elif data_type == "study":
            get_objects = self.get_studies(reference_mapping, container)
        elif data_type == "assay":
            get_objects = self.get_assays(reference_mapping, container)
        elif data_type == "data":
            get_objects = self.get_datas(reference_mapping, container)
        return get_objects

    def get_data_object(self, data_type, obj_id):
        if data_type == "investigation":
            get_object = self.get_investigation(obj_id)
        elif data_type == "study":
            get_object = self.get_study(obj_id)
        elif data_type == "assay":
            get_object = self.get_assay(obj_id)
        elif data_type == "data":
            get_object = self.get_data(obj_id)
        return get_object

    def parse_experiment(self, data_url, experiment_id):
        url = "{url_api}/experiments/{experiment_id}.json?token={token}".format(
            url_api=self.get_api_url(), experiment_id=experiment_id, token=self.token
        )
        experiment = requests.get(url)
        contains_element = 0
        contains_element_id = ""
        try:
            LabGuruConnector.check_status_code(experiment.status_code)
        except Exception as e:
            raise e
        experiment_json = experiment.json()
        experiment_procedures = experiment_json["experiment_procedures"]
        for item in experiment_procedures:
            experiment_procedure = item["experiment_procedure"]
            for element in experiment_procedure["elements"]:
                element_id = element["id"]
                url_element = (
                    "{url_api}/elements/{element_id}.json?token={token}".format(
                        url_api=self.get_api_url(),
                        element_id=element_id,
                        token=self.token,
                    )
                )
                element_get = requests.get(url_element)
                try:
                    LabGuruConnector.check_status_code(element_get.status_code)
                except Exception as e:
                    raise e
                element_get_json = element_get.json()

                if element_get_json["data"].find(str(data_url)) == -1:
                    continue
                else:
                    contains_element = 1
                    contains_element_id = element_id
                    pass
        if contains_element == 1:
            return contains_element_id
        else:
            return contains_element

    def add_text_in_section(self, data_name, data_url, experiment_id):
        jobid = utils.start_async_task()
        try:
            data_hyperlink = '<a href="{link}">{text}</a><br>'.format(
                link=data_url, text=data_url
            )
            contains_element = self.parse_experiment(data_url, experiment_id)
            text = str(data_name) + ": " + data_hyperlink
            url_api = self.get_api_url()
            url = "{url_api}/experiments/{experiment_id}.json?token={token}".format(
                url_api=url_api, experiment_id=str(experiment_id), token=str(self.token)
            )
            experiment = requests.get(url)
            experiment_json = experiment.json()
            experiment_procedures = experiment_json["experiment_procedures"]
            experiment_procedure_id = ""
            if contains_element == 0:
                for item in experiment_procedures:
                    experiment_procedure = item["experiment_procedure"]
                    if experiment_procedure["name"] == "OpenLink":
                        experiment_procedure_id = str(experiment_procedure["id"])
                        if not experiment_procedure["elements"]:
                            element_response = requests.post(
                                os.path.join(self.get_api_url(), "elements"),
                                data={
                                    "item[container_id]": experiment_procedure_id,
                                    "item[container_type]": "ExperimentProcedure",
                                    "item[element_type]": "text",
                                    "item[data]": text,
                                    "token": self.token,
                                },
                            )
                            return element_response
                        else:
                            for element in experiment_procedure["elements"]:
                                element_id = element["id"]
                if experiment_procedure_id != "":
                    url_element = (
                        "{url_api}/elements/{element_id}.json?token={token}".format(
                            url_api=url_api,
                            element_id=str(element_id),
                            token=self.token,
                        )
                    )
                    element_get = requests.get(url_element)
                    try:
                        LabGuruConnector.check_status_code(element_get.status_code)
                    except Exception as e:
                        raise e
                    element_get_json = element_get.json()
                    new_data_mapping = str(element_get_json["data"]) + " " + text
                    element_response = requests.put(
                        self.get_api_url()
                        + "/elements/"
                        + str(element_id)
                        + ".json?token="
                        + str(self.token),
                        params={
                            "element[data]": new_data_mapping,
                            "token": self.token,
                        },
                    )

                else:
                    section_response = requests.post(
                        os.path.join(self.get_api_url(), "sections"),
                        data={
                            "item[container_id]": experiment_id,
                            "item[container_type]": "Projects::Experiment",
                            "item[name]": "OpenLink",
                            "token": self.token,
                        },
                    )
                    experiment_procedure_id = section_response.json()["id"]
                    element_response = requests.post(
                        os.path.join(self.get_api_url(), "elements"),
                        data={
                            "item[container_id]": experiment_procedure_id,
                            "item[container_type]": "ExperimentProcedure",
                            "item[element_type]": "text",
                            "item[data]": text,
                            "token": self.token,
                        },
                    )
            else:
                url_element = (
                    "{url_api}/elements/{element_id}.json?token={token}".format(
                        url_api=self.get_api_url(),
                        element_id=str(contains_element),
                        token=self.token,
                    )
                )
                element_get = requests.get(url_element)
                try:
                    LabGuruConnector.check_status_code(element_get.status_code)
                except Exception as e:
                    raise e
                element_get_json = element_get.json()
                new_data_mapping = str(element_get_json["data"]).replace(
                    data_url
                    + "</a> (dataset not associated with this experiment anymore)",
                    data_url + "</a>",
                )
                element_response = requests.put(
                    self.get_api_url()
                    + "/elements/"
                    + str(contains_element)
                    + ".json?token="
                    + str(self.token),
                    params={
                        "element[data]": new_data_mapping,
                        "token": self.token,
                    },
                )
            url_experiment = self.url + "/knowledge/experiments/" + str(experiment_id)
            result = "New dataset link added to labguru experiment " + str(
                url_experiment
            )
            utils.finish_async_task(jobid, result)
        except Exception as e:
            utils.error_async_task(jobid)
            return e

    def delete_text_in_experiment_section(self, data_url, experiment_id):
        jobid = utils.start_async_task()
        try:
            url_api = self.get_api_url()
            data_hyperlink = '<a href="{link}">{text}</a>'.format(
                link=data_url, text=data_url
            )
            contains_element_id = self.parse_experiment(data_url, experiment_id)
            if contains_element_id != 0:
                url_element = (
                    "{url_api}/elements/{element_id}.json?token={token}".format(
                        url_api=url_api,
                        element_id=str(contains_element_id),
                        token=self.token,
                    )
                )
                element_get = requests.get(url_element)
                utils.finish_async_task(jobid, element_get.status_code)
                try:
                    LabGuruConnector.check_status_code(element_get.status_code)
                except Exception as e:
                    raise e
                element_get_json = element_get.json()
                new_data_mapping = str(element_get_json["data"]).replace(
                    data_hyperlink,
                    data_hyperlink
                    + " (data not associated with this experiment anymore)",
                )
                element_response = requests.put(
                    self.get_api_url()
                    + "/elements/"
                    + str(contains_element_id)
                    + ".json?token="
                    + str(self.token),
                    params={
                        "element[data]": new_data_mapping,
                        "token": self.token,
                    },
                )
                url_experiment = (
                    self.url + "/knowledge/experiments/" + str(experiment_id)
                )
                result = "Link removed from labguru experiment " + str(url_experiment)
                utils.finish_async_task(jobid, result)
                return element_response
        except Exception as e:
            utils.error_async_task(jobid)
            return e

    def get_information(self, type, id):
        info = {}
        if type == "investigation":
            type = "projects"
        elif type == "study":
            type = "milestones"
        elif type == "assay":
            type = "experiments"
        url = "{url_api}{type}/{id}.json?token={token}".format(
            url_api=self.get_api_url(), type=type, id=id, token=self.token
        )
        r = requests.get(url)
        try:
            LabGuruConnector.check_status_code(r.status_code)
        except Exception as e:
            raise e
        info["tags"] = r.json()["tags"]
        info["user"] = []

        return info

    def download(self, object_id, path):
        return None

    def check_file_access(self, object_id):
        return None

    def option_traitement(self, champ, mapping_object):
        if champ != {} and champ["labguru_synchronize_data_with_experiment"] == "on":
            synch_param = MappingParam()
            synch_param.mapping_id = mapping_object
            synch_param.key = "synch"
            synch_param.value = "True"
            synch_param.save()

    @receiver(new_mapping_signal, sender=Data)
    def add_text(sender, **kwargs):
        try:
            new_item = kwargs["item"]
            assay = Data.get_parent(new_item.id)
            for a in assay:
                project = Assay.get_project(a.id).first()
                investigation = Assay.get_investigation(a.id).first()
                investigation_mapping = Mapping.objects.get(
                    foreign_id_obj=investigation.id,
                    tool_id__connector="LabGuruConnector",
                )
                assay_mapping = Mapping.objects.get(
                    foreign_id_obj=a.id, tool_id__connector="LabGuruConnector"
                )
                if MappingParam.objects.filter(
                    mapping_id=assay_mapping, key="synch"
                ).exists():
                    synch_param = MappingParam.objects.get(
                        mapping_id=assay_mapping, key="synch"
                    )
                    synch = synch_param.value
                elif MappingParam.objects.filter(
                    mapping_id=investigation_mapping, key="synch"
                ).exists():
                    synch_param = MappingParam.objects.get(
                        mapping_id=investigation_mapping, key="synch"
                    )
                    synch = synch_param.value
                else:
                    pass
                if synch == "True":
                    tool_map = Tool.objects.get(mapping=assay_mapping)
                    user = utils.get_user_from_inspect()
                    profile = Profile.objects.get(user=user)
                    user_vault_id = profile.vault_id
                    token = utils.get_worker_vault_token()
                    connector = tool_map.get_connector(token, user_vault_id)
                    assay_map_id = int(assay_mapping.object_id)
                    target = a
                    description = "Add links in e-notebook section"
                    item_name = kwargs["item"].name
                    url_link = kwargs["connector"].get_url_link_to_an_object(
                        "dataset",
                        kwargs["new_mapping"].object_id,
                    )
                    utils.add_async_task(
                        project,
                        user,
                        target,
                        description,
                        connector.add_text_in_section,
                        item_name,
                        url_link,
                        assay_map_id,
                    )
        except Exception as e:
            raise e

    @receiver(delete_signal, sender=Data)
    def delete_text(sender, **kwargs):
        to_del = kwargs["del_mapping"]
        to_del_connector = kwargs["connector"]
        parent = Data.get_parent(to_del.foreign_id_obj.id).first()
        project = Assay.get_project(parent.id).first()
        investigation = Assay.get_investigation(parent.id).first()
        investigation_mapping = Mapping.objects.get(
            foreign_id_obj=investigation.id, tool_id__connector="LabGuruConnector"
        )
        try:
            parent_map = Mapping.objects.get(
                foreign_id_obj=parent.id, tool_id__connector="LabGuruConnector"
            )
            if MappingParam.objects.filter(mapping_id=parent_map, key="synch").exists():
                synch_param = MappingParam.objects.get(
                    mapping_id=parent_map, key="synch"
                )
                synch = synch_param.value
            elif MappingParam.objects.filter(
                mapping_id=investigation_mapping, key="synch"
            ).exists():
                synch_param = MappingParam.objects.get(
                    mapping_id=investigation_mapping, key="synch"
                )
                synch = synch_param.value
            else:
                pass
            if synch == "True":
                tool = Tool.objects.get(
                    mapping__id=parent_map.id,
                    connector="LabGuruConnector",
                )
                user = utils.get_user_from_inspect()
                profile = Profile.objects.get(user=user)
                user_vault_id = profile.vault_id
                token = utils.get_worker_vault_token()
                connector = tool.get_connector(token, user_vault_id)
                description = "Delete text in e-notebook section"
                target = parent
                user = utils.get_user_from_inspect()
                url_link = to_del_connector.get_url_link_to_an_object(
                    "data", to_del.object_id
                )
                utils.add_async_task(
                    project,
                    user,
                    target,
                    description,
                    connector.delete_text_in_experiment_section,
                    url_link,
                    int(parent_map.object_id),
                )
        except Exception:
            return ""


ToolConnector.register(LabGuruConnector)
