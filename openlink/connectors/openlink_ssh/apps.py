from django.apps import AppConfig


class OpenlinkSshConfig(AppConfig):
    name = "openlink_ssh"
