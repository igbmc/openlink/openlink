import logging
import os
from openlink.core.lib import utils
import openlink.core.connector
import requests
from bioblend import galaxy
from django import forms
from openlink.core.connector import (
    AuthentificationError,
    ContainerDataObject,
    Mapper,
    ToolConnector,
    ToolForm,
    defaultError,
)
from openlink.core.models import Mapping

logger = logging.getLogger(__name__)

"""
Creation form
"""


class GalaxyForm(ToolForm):
    url = forms.URLField(label="Galaxy URL", max_length=100, required=True)
    login = forms.CharField(label="Login", max_length=100, required=True)
    password = forms.CharField(
        widget=forms.PasswordInput, label="Password", max_length=100, required=True
    )
    shared = forms.BooleanField(
        label="Allows asynchronous tasks",
        help_text="This option allows openlink to access your login and password while not connected to run multiple asynchronous tasks",
        required=False,
    )
    public_data_dict = {}
    private_data_dict = {}

    def clean(self):
        cleaned_data = super().clean()
        url = cleaned_data.get("url")
        login = cleaned_data.get("login")
        password = cleaned_data.get("password")
        try:
            GalaxyConnector.test_connection(url, login, password)
        except Exception as e:
            raise Exception(str(e))

        public_data_list = ("url", "")
        private_data_list = ("login", "password")

        self.public_data_dict = {
            k: cleaned_data[k] for k in public_data_list if k in cleaned_data
        }
        self.private_data_dict = {
            k: cleaned_data[k] for k in private_data_list if k in cleaned_data
        }

        return cleaned_data


"""
Form to change credentials.
Should only return private param
"""


class GalaxyEditCredentials(ToolForm):
    url = forms.URLField()
    login = forms.CharField(label="login", max_length=100, required=True)
    password = forms.CharField(
        widget=forms.PasswordInput, label="password", max_length=100, required=True
    )
    shared = forms.BooleanField(
        label="Allows asynchronous tasks",
        help_text="This option allows openlink to access your login and password while not connected to run multiple asynchronous tasks",
        required=False,
    )
    private_data_dict = {}

    def __init__(self, data, *args, **kwargs):
        url = kwargs["url"]
        kwargs.pop("url")
        super(GalaxyEditCredentials, self).__init__(data)
        self.fields["url"] = forms.URLField(
            label="Galaxy URL",
            max_length=100,
            required=True,
            initial=url,
            disabled=True,
        )

    def clean(self):
        cleaned_data = super().clean()
        url = cleaned_data.get("url")
        login = cleaned_data.get("login")
        password = cleaned_data.get("password")
        try:
            GalaxyConnector.test_connection(url, login, password)
        except Exception as e:
            raise Exception(str(e))
        private_data_list = ("login", "password")
        self.private_data_dict = {
            k: cleaned_data[k] for k in private_data_list if k in cleaned_data
        }

        return cleaned_data


class GalaxyConnector(Mapper):
    def __init__(self, tool, token, user_vault_id=None):
        self.url = tool.get_public_param("url")
        self.login = tool.get_private_param("login", token, user_vault_id)
        self.password = tool.get_private_param("password", token, user_vault_id)
        self.tool = tool

    #
    # Default fonction
    #

    @classmethod
    def get_name(cls):
        return "Galaxy"

    @classmethod
    def get_data_structure(cls):
        return openlink.core.connector.LIST_STRUCTURE

    @classmethod
    def has_access_url(cls):
        return True

    @classmethod
    def get_supported_types(cls, data_object=None, tool=None):
        list_supported_types = {"assay": ["history"], "data": ["data"]}
        if data_object:
            parent = data_object.get_parent(data_object.id)
            try:
                Mapping.objects.get(foreign_id_obj=parent[0], tool_id=tool)
            except Mapping.DoesNotExist:
                if data_object.__class__.__name__.lower() != "assay":
                    list_supported_types.pop(
                        data_object.__class__.__name__.lower(), None
                    )
        return list_supported_types

    def get_plural_form_of_type(cls, type):
        plural_form = {"history": "histories", "data": "datas"}
        return plural_form[type]

    @classmethod
    def get_creation_form(cls, data=None):
        return GalaxyForm(data)

    @classmethod
    def get_addlink_form(cls, data, *args, **kwargs):
        return GalaxyEditCredentials(data, *args, **kwargs)

    @classmethod
    def get_logo(cls):
        logo = "images/logo-galaxy.png"
        return logo

    @classmethod
    def get_color(cls):
        color = "#7FAEE5"
        return color

    @classmethod
    def check_status_code(cls, r):
        if r != requests.codes.ok:
            if r == 401:
                raise AuthentificationError(cls, "email or password")
            else:
                raise defaultError(cls)

    @classmethod
    def test_connection(cls, url, email, password):
        logger.debug(email)
        logger.debug(password)
        logger.debug(url)
        gi = galaxy.GalaxyInstance(url=url, email=email, password=password, verify=True)
        cls.check_status_code(gi.make_get_request(url=gi.base_url).status_code)

    @classmethod
    def has_mapping_options(cls):
        return False

    def get_url_link_to_an_object(self, obj_type, obj_id):
        if obj_type == "assay":
            obj_type = "history"
            url = os.path.join(self.url + f"history/switch_to_history?hist_id={obj_id}")
        elif obj_type == "data":
            obj_type = "data"
            url = self.url
        return url

    def download(self, object_id, path):
        gi = self.get_galaxy_instance()
        try:
            di = gi.datasets.show_dataset(dataset_id=object_id.strip())
            name = di["name"]
            gi.datasets.download_dataset(
                dataset_id=object_id.strip(),
                file_path=path + "/" + name,
                use_default_filename=False,
                require_ok_state=False,
            )
        except Exception as e:
            raise e

    def check_file_access(self, object_id):
        try:
            gi = self.get_galaxy_instance()
        except Exception as e:
            logger.debug(e)
            access = False
        try:
            gi.datasets.show_dataset(dataset_id=object_id.strip())
            access = True
        except Exception as e:
            logger.debug(e)
            access = False
        return access

    def get_data_object(self, data_type, object_id):
        if data_type == "assay":
            get_object = self.get_assay(object_id)
        elif data_type == "data":
            get_object = self.get_data(object_id)
        return get_object

    def get_data_objects(self, data_type, reference_mapping=None, container=None):
        if data_type == "assay":
            get_objects = self.get_assays(reference_mapping, container)
        elif data_type == "data":
            get_objects = self.get_datas(reference_mapping, container)
        return get_objects

    def get_studies(self, reference_mapping, container):
        return None

    def get_study(self, object_id):
        return None

    def get_assays(self, reference_mapping, container):
        gi = self.get_galaxy_instance()
        histories = []
        hi = gi.histories.get_histories()
        for h in hi:
            histories.append(
                ContainerDataObject(
                    h["id"],
                    h["name"],
                    "history",
                )
            )
        return histories

    def get_assay(self, object_id):
        gi = self.get_galaxy_instance()
        hi = gi.histories.show_history(history_id=object_id)
        obj_to_link = ContainerDataObject(
            hi["id"],
            hi["name"],
            "history",
        )
        return obj_to_link

    def get_data(self, object_id):
        gi = self.get_galaxy_instance()
        di = gi.datasets.show_dataset(dataset_id=object_id.strip())
        if not di["deleted"]:
            obj_to_link = ContainerDataObject(
                di["id"], di["name"], "data", size=di["file_size"]
            )

        return obj_to_link

    def get_datas(self, reference_mapping, container):
        datas = []
        gi = self.get_galaxy_instance()
        di = gi.datasets.get_datasets(history_id=container.id)
        for d in di:
            if not d["deleted"]:
                ds = gi.datasets.show_dataset(dataset_id=str(d["id"]))
                size = ds["file_size"]
                datas.append(
                    ContainerDataObject(d["id"], d["name"], "data", size=int(size))
                )
        return datas

    def get_information(self, type, id):
        return {}

    def get_investigation(self, object_id):
        return []

    def get_investigations(self):
        return []

    def get_space_info(self, objects_id, mapping_object=None):
        if mapping_object:
            jobid = utils.start_async_task()
        try:
            size = None
            gi = self.get_galaxy_instance()
            data = gi.datasets.show_dataset(dataset_id=objects_id[0])
            size = data["file_size"]
            if mapping_object:
                mapping_object.size = size
                mapping_object.save()
                utils.finish_async_task(jobid, size)
            else:
                return size
        except Exception as e:
            utils.error_async_task(jobid)
            return e

    def get_galaxy_instance(self):
        gi = galaxy.GalaxyInstance(
            url=self.url, email=self.login, password=self.password, verify=True
        )
        try:
            self.check_status_code(gi.make_get_request(url=gi.base_url).status_code)
        except Exception as e:
            raise e
        return gi


ToolConnector.register(GalaxyConnector)
