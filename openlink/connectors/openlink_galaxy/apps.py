from django.apps import AppConfig


class OpenlinkGalaxyConfig(AppConfig):
    name = "openlink_galaxy"
